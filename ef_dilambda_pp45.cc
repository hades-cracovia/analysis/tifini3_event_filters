/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Rafał Lalik <rafal.lalik@ph.tum.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "ef_dilambda_pp45.h"

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <getopt.h>

#include <hphysicsconstants.h>
#include <hparticlecandsim.h>
#include <hparticlecand.h>
#include <hparticletool.h>
#include <hforwardcand.h>
#include <hgeantkine.h>
#include <hcategorymanager.h>
#include <hfrpchit.h>
#include <frpcdef.h>
#include <hforwardtools.h>

#include "KTools.h"
#include "KTrackInspector.h"
#include "KBeamCalibration.h"
#include "KCutInside.h"


#ifdef SHOWREJECTED
//int fRejected;
#endif /*SHOWREJECTED*/

static const float E_kin_beam       = 4500.0;
static const float E_kin_target     = 0.0;
static const float E_total_beam     = E_kin_beam + HPhysicsConstants::mass(14);
static const float E_total_target   = E_kin_target + HPhysicsConstants::mass(14);
static const float pz_beam          = sqrt(E_total_beam*E_total_beam-HPhysicsConstants::mass(14)*HPhysicsConstants::mass(14));

static const float LAMBDA_MASS = 1115.9;

// static const float D2R = TMath::DegToRad();
// static const float R2D = TMath::RadToDeg();

static AnaDataSet g_ads, g_adsXi;

float calcAngleVar(HGeomVector & v1, HGeomVector & v2)
{
    TVector3 _v1; _v1.SetXYZ(v1.X(), v1.Y(), v1.Z());
    TVector3 _v2; _v2.SetXYZ(v2.X(), v2.Y(), v2.Z());

    return _v1.Angle(_v2);
}

float calcAngleVar(HGeomVector & v1, TLorentzVector & v2)
{
    TVector3 _v1; _v1.SetXYZ(v1.X(), v1.Y(), v1.Z());
    TVector3 _v2; _v2.SetXYZ(v2.Px(), v2.Py(), v2.Pz());

    _v1.SetMag(1);
    _v2.SetMag(1);
    return _v1.Angle(_v2);
}

ef_dilambda_pp45::ef_dilambda_pp45(const TString& analysisName, const TString& treeName)
    : KAbstractAnalysis(analysisName, treeName)
    , flag_nosecvertcuts(0), flag_elosscorr(0), flag_nosigmas(0)
    , flag_useeventvertex(0), flag_usewall(0), flag_refit_fwdet(0)
    , par_Mtd(10), par_VertDistX(45), par_VertDistA(5), par_VertDistB(15)
{
//     setGoodEventSelector(Particle::kGoodTRIGGER |
//         Particle::kGoodVertexClust |
// //         Particle::kGoodVertexCand |
//         Particle::kGoodSTART |
//         Particle::kNoPileUpSTART);

    setExperimentType(KT::pp45);
    setGoodEventSelector(0);

    setPidSelectionHades(KT::p, KT::Beta | KT::Charge);
    setPidSelectionHades(KT::pim, KT::Beta | KT::Charge);

    eLossCorr = new HEnergyLossCorrPar("eLossCorr", "eLossCorr", "eLossCorr");
    eLossCorr->setDefaultPar("jan04");
}

bool ef_dilambda_pp45::analysis(HEvent * fEvent, Int_t event_num, Int_t /*run_id*/)
{
//     printf("%d  %d\n", cand_size, vect_size);
    if ( (hades_tracks.size() + forward_tracks.size()) < 4 )
        return false;

    int A_PID = KT::p;    // proton
    int B_PID = KT::pim;    // pi-

    g_ads.fHadesTracksNum = hades_tracks.size();
    g_ads.fFwDetTracksNum = forward_tracks.size();
    g_ads.fIsFwDetData[0] = 0;
    g_ads.fIsFwDetData[1] = 0;

    g_ads.fGeantWeight = 0;

    g_ads.fEventVertexX = fEvent->getHeader()->getVertexCluster().getX();
    g_ads.fEventVertexY = fEvent->getHeader()->getVertexCluster().getY();
    g_ads.fEventVertexZ = fEvent->getHeader()->getVertexCluster().getZ();

    g_ads.fEventLCounter = 0;

    g_ads.fMultA[0] = g_ads.fMultA[1] = 0;
    g_ads.fMultB[0] = g_ads.fMultB[1] = 0;

    std::vector<AnaDataSet> ads_arr;
    ads_arr.reserve(10000);

    size_t combo_cnt = 0;

    beamVector = refBeamVector;
//     if (analysisType == KT::Exp)
//     {
//         beamVector = beamCal->calculateBeamOffset(event->getRunId());
//     } else {
        beamVector = refBeamVector;
//     }

    TVector3 p_beam_vec(0.0, 0.0, pz_beam);
    TLorentzVector Vec_pp45_beam   = TLorentzVector(p_beam_vec.X(), p_beam_vec.Y(), p_beam_vec.Z(), E_total_beam);
    TLorentzVector Vec_pp45_target = TLorentzVector(0.0, 0.0, 0.0, E_total_target);
    Vec_pp45_sum = Vec_pp45_beam + Vec_pp45_target;


    // loops on each combination: p(L1)-pi(L1)-p(L2)-pi(L2)
    // where: p(L1/2) and pi(L1/2) comes from Lambda1 and Lambda2 respectively
    // fields can be H(ades) or F(wDet)

    // Lambda1
    // H-
    // proton in Hades
    for(int i = 0; i < g_ads.fHadesTracksNum; ++i)
    {
        // H-H-
        // pion in Hades
        for(int j = 0; j < g_ads.fHadesTracksNum; ++j)
        {
            // proton and pion must be different tracks
            if (i == j)
                continue;

            // check if proton and pion fullfill cuts
            if (!(hades_tracks[i].pid[A_PID][KT::Beta] and hades_tracks[i].pid[A_PID][KT::Charge]))
                break;

            if (!(hades_tracks[j].pid[B_PID][KT::Beta] and hades_tracks[j].pid[B_PID][KT::Charge]))
                continue;

            AnaDataSet ads_l1_ret = singleHadesPairAnalysis(fEvent, event_num, g_ads, A_PID, B_PID, i, j, 0);
            ads_l1_ret.fIsFwDetData[0] = 0;

            if (ads_l1_ret.ret == ERR_MASS_OUT_OF_RANGE)
                continue;

            if (ads_l1_ret.ret <= NULL_DATA)
                continue;

            // Lambda2
            // H-H-H-
            for(int k = 0; k < g_ads.fHadesTracksNum; ++k)
            {
                // can't use the same tracks in the event
                if (k == j or k == i)
                    continue;

                // H-H-H-H
                for(int l = 0; l < g_ads.fHadesTracksNum; ++l)
                {
                    // only for all tracks in Hades
                    if (k < i)     // k must be > i, otherwise repeated combinations
                        break;

                    if (l == k or l == j or l == i)
                        continue;

                    if (!(hades_tracks[k].pid[A_PID][KT::Beta] and hades_tracks[k].pid[A_PID][KT::Charge]))
                        continue;

                    if (!(hades_tracks[l].pid[B_PID][KT::Beta] and hades_tracks[l].pid[B_PID][KT::Charge]))
                        continue;

                    AnaDataSet ads_l2_ret = singleHadesPairAnalysis(fEvent, event_num, ads_l1_ret, A_PID, B_PID, k, l, 1);
                    ads_l2_ret.fIsFwDetData[1] = 0;

                    if (ads_l2_ret.ret == ERR_MASS_OUT_OF_RANGE)
                        continue;

                    if (ads_l2_ret.ret <= NULL_DATA)
                        continue;

                    hades_tracks[i].is_used = true;
                    hades_tracks[j].is_used = true;
                    hades_tracks[k].is_used = true;
                    hades_tracks[l].is_used = true;

                    ads_arr.push_back(ads_l2_ret);
                    ++combo_cnt;
                }

                for(int l = 0; l < g_ads.fFwDetTracksNum; ++l)
                {
                    // HADES track as particle A, FwDet as particle B
                    // H-H-H-F
                    if (hades_tracks[k].pid[A_PID][KT::Beta] and hades_tracks[k].pid[A_PID][KT::Charge])
                    {
                        AnaDataSet ads_l2_ret = singleFwDetPairAnalysis(fEvent, event_num, ads_l1_ret, A_PID, B_PID, k, l, 1);
                        ads_l2_ret.fIsFwDetData[1] = 0x02;

                        if (ads_l2_ret.ret == ERR_MASS_OUT_OF_RANGE)
                            continue;

                        if (ads_l2_ret.ret <= NULL_DATA)
                            continue;

                        hades_tracks[i].is_used = true;
                        hades_tracks[j].is_used = true;
                        hades_tracks[k].is_used = true;
                        forward_tracks[l].is_used = true;

                        ads_arr.push_back(ads_l2_ret);
                        ++combo_cnt;
                    }
                    // H-H-F-H
                    // HADES track as particle B
                    if (hades_tracks[k].pid[B_PID][KT::Beta] and hades_tracks[k].pid[B_PID][KT::Charge])
                    {
                        AnaDataSet ads_l2_ret = singleFwDetPairAnalysis(fEvent, event_num, ads_l1_ret, B_PID, A_PID, k, l, 1);
                        ads_l2_ret.fIsFwDetData[1] = 0x01;

                        if (ads_l2_ret.ret == ERR_MASS_OUT_OF_RANGE)
                            continue;

                        if (ads_l2_ret.ret <= NULL_DATA)
                            continue;

                        hades_tracks[i].is_used = true;
                        hades_tracks[j].is_used = true;
                        hades_tracks[k].is_used = true;
                        forward_tracks[l].is_used = true;

                        ads_arr.push_back(ads_l2_ret);
                            ++combo_cnt;
                    }
                }
            }
        }


        for(int j = 0; j < g_ads.fFwDetTracksNum; ++j)
        {
            // H-F-
            if (hades_tracks[i].pid[A_PID][KT::Beta] and hades_tracks[i].pid[A_PID][KT::Charge])
            {
                AnaDataSet ads_l1_ret = singleFwDetPairAnalysis(fEvent, event_num, g_ads, A_PID, B_PID, i, j, 0);
                ads_l1_ret.fIsFwDetData[0] = 0x02;

                if (ads_l1_ret.ret == ERR_MASS_OUT_OF_RANGE)
                    continue;

                if (ads_l1_ret.ret <= NULL_DATA)
                    continue;

                // Lambda2
                for(int k = 0; k < g_ads.fHadesTracksNum; ++k)
                {
                    if (k == i)
                        continue;

                    for(int l = 0; l < g_ads.fFwDetTracksNum; ++l)
                    {
                        if (l == j)
                            continue;

                        // H-F-H-F
                        // HADES track as particle A
                        if (hades_tracks[k].pid[A_PID][KT::Beta] and hades_tracks[k].pid[A_PID][KT::Charge] and (k > i))
                        {
                            AnaDataSet ads_l2_ret = singleFwDetPairAnalysis(fEvent, event_num, ads_l1_ret, A_PID, B_PID, k, l, 1);
                            ads_l2_ret.fIsFwDetData[1] = 0x02;

                            if (ads_l2_ret.ret == ERR_MASS_OUT_OF_RANGE)
                                continue;

                            if (ads_l2_ret.ret <= NULL_DATA)
                                continue;

                            hades_tracks[i].is_used = true;
                            forward_tracks[j].is_used = true;
                            hades_tracks[k].is_used = true;
                            forward_tracks[l].is_used = true;

                            ads_arr.push_back(ads_l2_ret);
                            ++combo_cnt;
                        }
                        // H-F-F-H
                        // HADES track as particle B
                        if (hades_tracks[k].pid[B_PID][KT::Beta] and hades_tracks[k].pid[B_PID][KT::Charge])
                        {
                            AnaDataSet ads_l2_ret = singleFwDetPairAnalysis(fEvent, event_num, ads_l1_ret, B_PID, A_PID, k, l, 1);
                            ads_l2_ret.fIsFwDetData[1] = 0x01;

                            if (ads_l2_ret.ret == ERR_MASS_OUT_OF_RANGE)
                                continue;

                            if (ads_l2_ret.ret <= NULL_DATA)
                                continue;

                            hades_tracks[i].is_used = true;
                            forward_tracks[j].is_used = true;
                            hades_tracks[k].is_used = true;
                            forward_tracks[l].is_used = true;

                            ads_arr.push_back(ads_l2_ret);
                            ++combo_cnt;
                        }
                    }
                }
            }

            // F-H-
            if (hades_tracks[i].pid[B_PID][KT::Beta] and hades_tracks[i].pid[B_PID][KT::Charge])
            {
                AnaDataSet ads_l1_ret = singleFwDetPairAnalysis(fEvent, event_num, g_ads, B_PID, A_PID, i, j, 0);
                ads_l1_ret.fIsFwDetData[0] = 0x01;

                if (ads_l1_ret.ret == ERR_MASS_OUT_OF_RANGE)
                    continue;

                if (ads_l1_ret.ret <= NULL_DATA)
                    continue;

                // Lambda2
                for(int k = 0; k < g_ads.fHadesTracksNum; ++k)
                {
                    if (k == i)
                        continue;

                    for(int l = 0; l < g_ads.fFwDetTracksNum; ++l)
                    {
                        if (l == j)
                            continue;

                        // F-H-H-F
                        // HADES track as particle A
                        if (hades_tracks[k].pid[A_PID][KT::Beta] and hades_tracks[k].pid[A_PID][KT::Charge])
                        {
                            AnaDataSet ads_l2_ret = singleFwDetPairAnalysis(fEvent, event_num, ads_l1_ret, A_PID, B_PID, k, l, 1);
                            ads_l2_ret.fIsFwDetData[1] = 0x02;

                            if (ads_l2_ret.ret == ERR_MASS_OUT_OF_RANGE)
                                continue;

                            if (ads_l2_ret.ret <= NULL_DATA)
                                continue;

                            hades_tracks[i].is_used = true;
                            forward_tracks[j].is_used = true;
                            hades_tracks[k].is_used = true;
                            forward_tracks[l].is_used = true;

                            ads_arr.push_back(ads_l2_ret);
                            ++combo_cnt;
                        }
                        // F-H-F-H
                        // HADES track as particle B
                        if (hades_tracks[k].pid[B_PID][KT::Beta] and hades_tracks[k].pid[B_PID][KT::Charge] and (k > i))
                        {
                            AnaDataSet ads_l2_ret = singleFwDetPairAnalysis(fEvent, event_num, ads_l1_ret, B_PID, A_PID, k, l, 1);
                            ads_l2_ret.fIsFwDetData[1] = 0x01;

                            if (ads_l2_ret.ret == ERR_MASS_OUT_OF_RANGE)
                                continue;

                            if (ads_l2_ret.ret <= NULL_DATA)
                                continue;

                            hades_tracks[i].is_used = true;
                            forward_tracks[j].is_used = true;
                            hades_tracks[k].is_used = true;
                            forward_tracks[l].is_used = true;

                            ads_arr.push_back(ads_l2_ret);
                            ++combo_cnt;
                        }
                    }
                }
            }
        }
    }

    for (size_t i = 0; i < combo_cnt; ++i)
    {
        g_ads = ads_arr[i];

        // copying
        for (int ln = 0; ln < 2; ++ln)
        {
            track_lambda_cms[ln] = ads_arr[i].tr_lambda_cms[ln];
            track_lambda_a[ln] = ads_arr[i].tr_lambda_a[ln];
            track_lambda_b[ln] = ads_arr[i].tr_lambda_b[ln];

            vertex_lambda[ln] = ads_arr[i].vx_lambda[ln];

            trec_lambda[ln] = ads_arr[i].trec_lambda[ln];
        }
        trec_lala = ads_arr[i].trec_lala;
        vertex_primary = ads_arr[i].vx_primary;

        // filling
        for (int ln = 0; ln < 2; ++ln)
        {
            track_lambda_cms[ln].fill();
            track_lambda_a[ln].fill();
            track_lambda_b[ln].fill();

            vertex_lambda[ln].fill();
            trec_lambda[ln].fill();
        }

        vertex_primary.fill();

        getTree()->Fill();
    }

    return true;
}

AnaDataSet ef_dilambda_pp45::singleHadesPairAnalysis(HEvent * /*fEvent*/, int /*event_num*/, const AnaDataSet & ads_a, UInt_t pid_a, UInt_t pid_b, int trackA_num, int trackB_num, int ln, bool /*quick_run*/)
{
    AnaDataSet ads = ads_a;
    if (0 == ln) ads.init();

//     ads.fGeantWeight = pcand->getGeantGenweight(); FIXME

    HGeomVector dirMother;

    TObject * o_a = hades_tracks[trackA_num].cand;
    TObject * o_b = hades_tracks[trackB_num].cand;

    HParticleCand trackA = *(HParticleCand*)o_a;
    HParticleCand trackB = *(HParticleCand*)o_b;

    trackA.calc4vectorProperties(HPhysicsConstants::mass(pid_a));
    trackB.calc4vectorProperties(HPhysicsConstants::mass(pid_b));

    Float_t fMomA = trackA.getMomentum();
    Float_t fMomB = trackB.getMomentum();

    if (flag_elosscorr and analysisType == KT::Exp)
    {
        float momentum_A_corr = eLossCorr->getCorrMom(pid_a, fMomA, trackA.getTheta());
        float momentum_B_corr = eLossCorr->getCorrMom(pid_b, fMomB, trackB.getTheta());
        trackA.setMomentum(momentum_A_corr);
        trackB.setMomentum(momentum_B_corr);
    }

    trackA.calc4vectorProperties(HPhysicsConstants::mass(pid_a));
    trackB.calc4vectorProperties(HPhysicsConstants::mass(pid_b));

    ads.tr_lambda_a[ln] = trackA;
    ads.tr_lambda_b[ln] = trackB;
    ads.trec_lambda[ln].reconstruct(trackA, trackB);
    ads.fLambda_MTD[ln] = ads.trec_lambda[ln].getMTD();        // minimum distance between the two tracks

    // I do not need so many data!
    if (ads.trec_lambda[ln].M() > 1200)
    {
    #ifdef SHOWREJECTED
        ads.fRejected = ERR_MASS_OUT_OF_RANGE;
    #else
        ads.ret = ERR_MASS_OUT_OF_RANGE;
        return ads;
    #endif /*SHOWREJECTED*/
    }

//     if (quick_run)
//     {
//         ads.ret = (LAMBDA_MASS - ads.trec_lambda[ln].M());
//         return ads;// * fMinTrackDist;
//     }

    float GeantxVertexA    = 0;
    float GeantyVertexA    = 0;
    float GeantzVertexA    = 0;

    // extra checks for the simulation analysis
    HVirtualCandSim * tcs_a = dynamic_cast<HVirtualCandSim*>(o_a);
    HVirtualCandSim * tcs_b = dynamic_cast<HVirtualCandSim*>(o_b);

    if (analysisType == KT::Sim && tcs_a && tcs_b)
    {
        int GeantPIDA = tcs_a->getGeantPID();
        int GeantPIDB = tcs_b->getGeantPID();

        int GeantPIDAparent = tcs_a->getGeantParentPID();
        int GeantPIDBparent = tcs_b->getGeantParentPID();

        int GeantPIDAGparent = tcs_a->getGeantGrandParentPID();
        int GeantPIDBGparent = tcs_b->getGeantGrandParentPID();

        int GeantTrackAparent = tcs_a->getGeantParentTrackNum();
        int GeantTrackBparent = tcs_b->getGeantParentTrackNum();

        int GeantTrackBGparent = tcs_b->getGeantGrandParentTrackNum();

        ads.fRealLambda[ln] = (
            // proton-pim pair
            (GeantPIDA == 14 and GeantPIDAparent == 18 and GeantPIDB == 9 and GeantPIDBparent == 18 && GeantTrackAparent == GeantTrackBparent)
            or
            // proton-pi- (decaying into mu-)
            (GeantPIDA == 14 and GeantPIDAparent == 18 and GeantPIDB == 6 and GeantPIDBparent == 9 and GeantPIDBGparent == 18 and GeantTrackAparent == GeantTrackBGparent)
        );

        ads.fDecayMech[ln] = tcs_a->getGeantCreationMechanism();
        ads.fPrimLambda[ln] = ads.fRealLambda[ln] && (GeantPIDAGparent == 0);

        HCategory* fCatCand = (HCategory*)HCategoryManager::getCategory(catGeantKine, 1, "HGeantKine");
        if (fCatCand)
        {
            for (int i = 0; i < fCatCand->getEntries(); ++i)
            {
                HGeantKine * kine = (HGeantKine *) fCatCand->getObject(i);
                if (kine && kine->getTrack() == tcs_a->getGeantParentTrackNum())
                    ads.fCreationMech[ln] = kine->getMechanism();
            }
        }
        else
        {
            ads.fCreationMech[ln] = 0;
        }
    }
    else
    {
        ads.fPrimLambda[ln] = 0;
        ads.fRealLambda[ln] = 0;
        ads.fCreationMech[ln] = 0;
        ads.fDecayMech[ln] = -1;
    }

    ads.fChiA[ln] = trackA.getChi2();
    ads.fChiB[ln] = trackB.getChi2();

    ads.fMetaMatchQA[ln] = trackA.getMetaMatchQuality();
    ads.fMetaMatchQB[ln] = trackB.getMetaMatchQuality();

    dirMother.setXYZ(ads.trec_lambda[ln].X(), ads.trec_lambda[ln].Y(), ads.trec_lambda[ln].Z());    // direction vector of the mother particle
    ads.vx_lambda[ln] = ads.trec_lambda[ln].getDecayVertex();

    if (analysisType == KT::Sim)
    {
        ads.fDVres = TMath::Sqrt(
                TMath::Power(ads.vx_lambda[ln].X() - GeantxVertexA, 2) +
                TMath::Power(ads.vx_lambda[ln].Y() - GeantyVertexA, 2) +
                TMath::Power(ads.vx_lambda[ln].Z() - GeantzVertexA, 2)
        );
    }

    ads.fVertDistA[ln] = ads.trec_lambda[ln].getMTDa();
    ads.fVertDistB[ln] = ads.trec_lambda[ln].getMTDb();

//     if ( !(ads.vx_primary.getR() < 10.0 and ads.vx_primary.getZ() < 0.0 and ads.vx_primary.getZ() > -90.0) )
//     {
// #ifdef SHOWREJECTED
//         ads.fRejected = ERR_NOT_IN_VERTEX;
// #else
//         ads.ret = ERR_NOT_IN_VERTEX;
//         return ads;
// #endif /*SHOWREJECTED*/
//     }

    ads.fAngleAB    = trackA.Angle(trackB.Vect());
    ads.fRelAngleA  = ads.trec_lambda[ln].Angle(trackA.Vect());
    ads.fRelAngleB  = ads.trec_lambda[ln].Angle(trackB.Vect());

    //Boost in CMS: ***********************************************
    TLorentzVector trackAB_CMS = ads.trec_lambda[ln];

    trackAB_CMS.Boost(-Vec_pp45_sum.BoostVector());
    ads.tr_lambda_cms[ln] = trackAB_CMS;

    TLorentzVector trackA_CMS = trackA;
    trackA_CMS.Boost(-Vec_pp45_sum.BoostVector());
    ads.fMomA_cms[ln]            = trackA_CMS.P();

    TLorentzVector trackB_CMS = trackB;
    trackB_CMS.Boost(-Vec_pp45_sum.BoostVector());
    ads.fMomB_cms[ln]            = trackB_CMS.P();

    //*************************************************************

    ads.fLambda_MM[ln] = (Vec_pp45_sum - ads.trec_lambda[ln]).M2();      // Missing Mass

    if (ln == 1)
    {
        ads.trec_lala.reconstruct(ads.trec_lambda[0], ads.trec_lambda[1]);
        ads.fLambda_MTD[2] = ads.trec_lala.getMTD();            // minimum distance between the two tracks
        ads.fLambda_MM[2] = (Vec_pp45_sum - ads.trec_lala).M2(); // Missing Mass of all

        // Primary vertex and topological vriables
        ads.vx_primary = ads.trec_lala.getDecayVertex();

        // distance between Primary and Lambda1
        HGeomVector v1 = ads.vx_lambda[0] - ads.vx_primary;
        ads.fVertDistX[0] = (v1).length();
        ads.fLambda_PVA[0] = calcAngleVar(v1, ads.trec_lambda[0]);

        // distance between Primary and Lambda2
        HGeomVector v2 = ads.vx_lambda[1] - ads.vx_primary;
        ads.fVertDistX[1] = (v2).length();
        ads.fLambda_PVA[1] = calcAngleVar(v2, ads.trec_lambda[1]);
    }

    ++ads.fEventLCounter;

    ads.ret = ads.trec_lambda[ln].M();
    return ads;
}

AnaDataSet ef_dilambda_pp45::singleFwDetPairAnalysis(HEvent * /*fEvent*/, Int_t /*event_num*/, const AnaDataSet & ads_a, UInt_t pid_a, UInt_t pid_b, int trackA_num, int trackB_num, int ln, bool /*quick_run*/)
{
    AnaDataSet ads = ads_a;
    if (0 == ln) ads.init();
//     ads.fGeantWeight = pcand->getGeantGenweight(); FIXME

    HGeomVector dirMother;

    TObject * o_a = hades_tracks[trackA_num].cand;
    TObject * o_b = forward_tracks[trackB_num].cand;

    HParticleCand trackA = *(HParticleCand*)o_a;
    HForwardCand trackB = *(HForwardCand*)o_b;

    if (trackB.getTofRec() == 0)
    {
    #ifdef SHOWREJECTED
        ads.fRejected = ERR_NO_FWDET_TOF;
    #else
        ads.ret = ERR_NO_FWDET_TOF;
        return ads;
    #endif /*SHOWREJECTED*/
    }

    trackA.calc4vectorProperties(HPhysicsConstants::mass(pid_a));
    trackB.calc4vectorProperties(HPhysicsConstants::mass(pid_b));

    Float_t fMomA = trackA.getMomentum();

    if (flag_elosscorr and analysisType == KT::Exp)
    {
        float momentum_A_corr = eLossCorr->getCorrMom(pid_a, fMomA, trackA.getTheta());
        trackA.setMomentum(momentum_A_corr);
    }

    trackA.calc4vectorProperties(HPhysicsConstants::mass(pid_a));

    ads.trec_lambda[ln].reconstruct(trackA, trackB);
    ads.vx_lambda[ln] = ads.trec_lambda[ln].getDecayVertex();

    if (flag_refit_fwdet && trackB.getTofRec())
    {
        HCategory * pFRpcHit = gHades->getCurrentEvent()->getCategory(catFRpcHit);
        HFRpcHit * frpchit = dynamic_cast<HFRpcHit*>(pFRpcHit->getObject(trackB.getFRpcHitIndex()));
        HForwardTools::correctPathLength(&trackB, HGeomVector(ads.vx_lambda[ln].X(), ads.vx_lambda[ln].Y(), ads.vx_lambda[ln].Z()), frpchit);
        HForwardTools::correctTrackProperties(&trackB, HPhysicsConstants::mass(14));

        ads.trec_lambda[ln].reconstruct(trackA, trackB);
        ads.vx_lambda[ln] = ads.trec_lambda[ln].getDecayVertex();
    }

    if (trackB.getTofRec() <= 0)
    {
    #ifdef SHOWREJECTED
        ads.fRejected = ERR_NO_FWDET_TOF;
    #else
        ads.ret = ERR_NO_FWDET_TOF;
        return ads;
    #endif /*SHOWREJECTED*/
    }

    ads.tr_lambda_a[ln] = trackA;
    ads.tr_lambda_b[ln] = trackB;

    ads.fpartB_tof[ln] = trackB.getTof();
    ads.fLambda_MTD[ln] = ads.trec_lambda[ln].getMTD();

    // we do not need so many data!
    if (ads.trec_lambda[ln].M() > 1200)
    {
    #ifdef SHOWREJECTED
        ads.fRejected = ERR_MASS_OUT_OF_RANGE;
    #else
        ads.ret = ERR_MASS_OUT_OF_RANGE;
        return ads;
    #endif /*SHOWREJECTED*/
    }

//     if (quick_run)
//     {
//         ads.ret = (LAMBDA_MASS - ads.trec_lambda[ln].M());
//         return ads;// * fMinTrackDist;
//     }

    float GeantxVertexA    = 0;
    float GeantyVertexA    = 0;
    float GeantzVertexA    = 0;

    // extra checks for the simulation analysis
    HVirtualCandSim * tcs_a = dynamic_cast<HVirtualCandSim*>(o_a);
    HVirtualCandSim * tcs_b = dynamic_cast<HVirtualCandSim*>(o_b);

    if (analysisType == KT::Sim && tcs_a && tcs_b)
    {
        int GeantPIDA = tcs_a->getGeantPID();
        int GeantPIDB = tcs_b->getGeantPID();

        int GeantPIDAparent = tcs_a->getGeantParentPID();
        int GeantPIDBparent = tcs_b->getGeantParentPID();

        int GeantPIDAGparent = tcs_a->getGeantGrandParentPID();
        int GeantPIDBGparent = tcs_b->getGeantGrandParentPID();

        int GeantTrackAparent = tcs_a->getGeantParentTrackNum();
        int GeantTrackBparent = tcs_b->getGeantParentTrackNum();

        int GeantTrackAGparent = tcs_a->getGeantGrandParentTrackNum();
        int GeantTrackBGparent = tcs_b->getGeantGrandParentTrackNum();

        Bool_t A_is_OK = kFALSE;
        Bool_t B_is_OK = kFALSE;
        Bool_t A_B_tracks_are_OK = kFALSE;

        if (14 == pid_a)
        {
            A_is_OK = GeantPIDAparent == 18;
        }
        else if (9 == pid_a)
        {
            A_is_OK = (GeantPIDA == 9 and GeantPIDAparent == 18) or
                        (GeantPIDA == 6 and GeantPIDAparent == 9 and GeantPIDAGparent == 18);
        }

        if (14 == pid_b)
        {
            B_is_OK = GeantPIDBparent == 18;
        }
        else if (9 == pid_b)
        {
            B_is_OK = (GeantPIDB == 9 and GeantPIDBparent == 18) or
                        (GeantPIDB == 6 and GeantPIDBparent == 9 and GeantPIDBGparent == 18);
        }

        if (14 == pid_a)
        {
            A_B_tracks_are_OK = (GeantTrackAparent == GeantTrackBparent or GeantTrackAparent == GeantTrackBGparent);
        }
        else
        {
            A_B_tracks_are_OK = (GeantTrackAparent == GeantTrackBparent or GeantTrackAGparent == GeantTrackBparent);
        }
        ads.fRealLambda[ln] = A_is_OK and B_is_OK and A_B_tracks_are_OK;
        ads.fpartB_tof_pid[ln] = GeantPIDB;
        ads.fDecayMech[ln] = tcs_a->getGeantCreationMechanism();
        ads.fPrimLambda[ln] = ads.fRealLambda[ln] && (GeantPIDAGparent == 0);

        HCategory* fCatCand = (HCategory*)HCategoryManager::getCategory(catGeantKine, 1, "HGeantKine");
        if (fCatCand)
        {
            for (int i = 0; i < fCatCand->getEntries(); ++i)
            {
                HGeantKine * kine = (HGeantKine *) fCatCand->getObject(i);
                if (kine && kine->getTrack() == tcs_a->getGeantParentTrackNum())
                    ads.fCreationMech[ln] = kine->getMechanism();
            }
        }
        else
        {
            ads.fCreationMech[ln] = 0;
        }
    }
    else
    {
        ads.fPrimLambda[ln] = 0;
        ads.fRealLambda[ln] = 0;
        ads.fCreationMech[ln] = 0;
        ads.fDecayMech[ln] = -1;
    }

    ads.fChiA[ln] = trackA.getChi2();
    ads.fChiB[ln] = trackB.getChi2();

    ads.fMetaMatchQA[ln] = trackA.getMetaMatchQuality();

    dirMother.setXYZ(ads.trec_lambda[ln].X(), ads.trec_lambda[ln].Y(), ads.trec_lambda[ln].Z());    // direction vector of the mother particle

    if (analysisType == KT::Sim)
    {
        ads.fDVres = TMath::Sqrt(
                TMath::Power(ads.vx_lambda[ln].X() - GeantxVertexA, 2) +
                TMath::Power(ads.vx_lambda[ln].Y() - GeantyVertexA, 2) +
                TMath::Power(ads.vx_lambda[ln].Z() - GeantzVertexA, 2)
        );
    }

//     ads.fVertDistA[ln] = ads.trec_lambda[ln].getMTDa();
//     ads.fVertDistB[ln] = ads.trec_lambda[ln].getMTDb();

//     ads.vx_primary = PrimVertexMother; FIXME
//     if ((ads.vx_lambda.Z() - ads.vx_primary.Z()) < 0)
//     {
// #ifdef SHOWREJECTED
//         ads.fRejected = ERR_VERTEX_Z_MISSMATCH;
// #else
// //         ads.ret = ERR_VERTEX_Z_MISSMATCH;            // FIXME and below
// //         return ads;
// #endif /*SHOWREJECTED*/
//     }

    ads.fVertDistA[ln] = ads.trec_lambda[ln].getMTDa();
    ads.fVertDistB[ln] = ads.trec_lambda[ln].getMTDb();

//     double dist2 = pow(PrimVertexMother.getX() - beamVector.getX(), 2.0) +
//         pow(PrimVertexMother.getY() - beamVector.getY(), 2.0);

//     if ( !(dist2 < 100.0 and PrimVertexMother.getZ() < 0.0 and PrimVertexMother.getZ() > -90.0) )
//         return ERR_NOT_IN_VERTEX;
//     if ( !(ads.vx_primary.getR() < 10.0 and ads.vx_primary.getZ() < 0.0 and ads.vx_primary.getZ() > -90.0) )
//     {
// #ifdef SHOWREJECTED
//         ads.fRejected = ERR_NOT_IN_VERTEX;
// #else
//         ads.ret = ERR_NOT_IN_VERTEX;
//         return ads;
// #endif /*SHOWREJECTED*/
//     }

    ads.fAngleAB    = trackA.Angle(trackB.Vect());
    ads.fRelAngleA  = ads.trec_lambda[ln].Angle(trackA.Vect());
    ads.fRelAngleB  = ads.trec_lambda[ln].Angle(trackB.Vect());

    //Boost in CMS: ***********************************************
    TLorentzVector trackAB_CMS = ads.trec_lambda[ln];

    trackAB_CMS.Boost(-Vec_pp45_sum.BoostVector());
    ads.tr_lambda_cms[ln] = trackAB_CMS;

    TLorentzVector trackA_CMS = trackA;
    trackA_CMS.Boost(-Vec_pp45_sum.BoostVector());
    ads.fMomA_cms[ln]     = trackA_CMS.P();

    TLorentzVector trackB_CMS = trackB;
    trackB_CMS.Boost(-Vec_pp45_sum.BoostVector());
    ads.fMomB_cms[ln]     = trackB_CMS.P();

    //*************************************************************

    ads.fLambda_MM[ln] = (Vec_pp45_sum - ads.trec_lambda[ln]).M2();      // Missing Mass

    if (ln == 1)
    {
        ads.trec_lala.reconstruct(ads.trec_lambda[0], ads.trec_lambda[1]);
        ads.fLambda_MTD[2] = ads.trec_lala.getMTD();            // minimum distance between the two tracks
        ads.fLambda_MM[2] = (Vec_pp45_sum - ads.trec_lala).M2(); // Missing Mass of all

        // Primary vertex and topological vriables
        ads.vx_primary = ads.trec_lala.getDecayVertex();

        // distance between Primary and Lambda1
        HGeomVector v1 = ads.vx_lambda[0] - ads.vx_primary;
        ads.fVertDistX[0] = (v1).length();
        ads.fLambda_PVA[0] = calcAngleVar(v1, ads.trec_lambda[0]);

        // distance between Primary and Lambda2
        HGeomVector v2 = ads.vx_lambda[1] - ads.vx_primary;
        ads.fVertDistX[1] = (v2).length();
        ads.fLambda_PVA[1] = calcAngleVar(v2, ads.trec_lambda[1]);
    }

    if (flag_nosecvertcuts == 0)
    {
    if (analysisType == KT::Sim)     //for simulated data
    {
    }
    else       //for experimental data
    {
    }
    }

    ++ads.fEventLCounter;

//     A_PID = KT::pip;
//     trackA = *(HParticleCandSim*)pcand->getObject(trackA_num);
//     trackA.calc4vectorProperties(HPhysicsConstants::mass(A_PID));
//     KTifini::CalcSegVector(trackA.getZ(), trackA.getR(), trackA.getPhi(), trackA.getTheta(), baseA, dirA);
//     ads.fMomA = trackA.getMomentum();
//
//     if (flag_elosscorr)
//     {
//         // with corr
//         momentum_A_corr = eLossCorr->getCorrMom(A_PID, ads.fMomA, trackA.getTheta());
//     }
//     else
//     {
//         // no corr
//         momentum_A_corr = ads.fMomA;
//     }
//
//     trackA.setMomentum(momentum_A_corr);
//
//     trackA.calc4vectorProperties(HPhysicsConstants::mass(A_PID));
//
//     TLorentzVector trackAB_miss = trackA + trackB;
//
//     ads.fM_miss = trackAB_miss.M();
//     ads.fPVA_miss = calcAngleVar(v1, trackAB_miss);
//
    ads.ret = ads.trec_lambda[ln].M();
    return ads;
}

void ef_dilambda_pp45::configureTree(TTree * tree)
{
    trec_lambda[0].setTree(tree, "Lambda1_", 0x2ff);
    track_lambda_cms[0].setTree(tree, "Lambda1_cms_", KTrack::bCosTheta | KTrack::bP | KTrack::bY);
    vertex_lambda[0].setTree(tree, "Lambda1Decay", KVertex::bXYZ | KVertex::bR);
    track_lambda_a[0].setTree(tree, "partA_", 0x1ff);
    track_lambda_b[0].setTree(tree, "partB_", 0x1ff);

    trec_lambda[1].setTree(tree, "Lambda2_", 0x2ff);
    track_lambda_cms[1].setTree(tree, "Lambda2_cms_", KTrack::bCosTheta | KTrack::bP | KTrack::bY);
    vertex_lambda[1].setTree(tree, "Lambda2Decay", KVertex::bXYZ | KVertex::bR);
    track_lambda_a[1].setTree(tree, "partC_", 0x1ff);
    track_lambda_b[1].setTree(tree, "partD_", 0x1ff);

    trec_lala.setTree(tree, "LaLa_", 0x002);

    vertex_primary.setTree(tree, "PrimaryVertex", KVertex::bXYZ | KVertex::bR);

    tree->Branch("fLambda1_MM",     &g_ads.fLambda_MM[0],   "fLambda1_MM/F" );
    tree->Branch("fLambda2_MM",     &g_ads.fLambda_MM[1],   "fLambda2_MM/F" );
    tree->Branch("fLambda_MM",      &g_ads.fLambda_MM[2],   "fLambda_MM/F" );

    tree->Branch("fLambda1_MTD",    &g_ads.fLambda_MTD[0],  "fLambda1_MTD/F" );
    tree->Branch("fLambda2_MTD",    &g_ads.fLambda_MTD[1],  "fLambda2_MTD/F" );
    tree->Branch("fLambda_MTD",     &g_ads.fLambda_MTD[2],  "fLambda_MTD/F" );
    tree->Branch("fVertDistX1",     &g_ads.fVertDistX[0],   "fVertDistX1/F");
    tree->Branch("fVertDistX2",     &g_ads.fVertDistX[1],   "fVertDistX2/F");
    tree->Branch("fLambda1_PVA",    &g_ads.fLambda_PVA[0],  "fLambda1_PVA/F");
    tree->Branch("fLambda2_PVA",    &g_ads.fLambda_PVA[1],  "fLambda2_PVA/F");

    tree->Branch("fFitVertexX",     &g_ads.fFitVertexX,     "fFitVertexX/F");
    tree->Branch("fFitVertexY",     &g_ads.fFitVertexY,     "fFitVertexY/F");
    tree->Branch("fFitVertexZ",     &g_ads.fFitVertexZ,     "fFitVertexZ/F");

    tree->Branch("fEventVertexX",   &g_ads.fEventVertexX,   "fEventVertexX/F");
    tree->Branch("fEventVertexY",   &g_ads.fEventVertexY,   "fEventVertexY/F");
    tree->Branch("fEventVertexZ",   &g_ads.fEventVertexZ,   "fEventVertexZ/F");

    tree->Branch("fDVres",          &g_ads.fDVres,          "fDVres/F");

    tree->Branch("fMomA_cms",       &g_ads.fMomA_cms[0],    "fMomA_cms/F");
    tree->Branch("fMomB_cms",       &g_ads.fMomB_cms[0],    "fMomB_cms/F");
    tree->Branch("fMomC_cms",       &g_ads.fMomA_cms[1],    "fMomC_cms/F");
    tree->Branch("fMomD_cms",       &g_ads.fMomB_cms[1],    "fMomD_cms/F");
    tree->Branch("fAngleAB",        &g_ads.fAngleAB,        "fAngleAB/F");
    tree->Branch("fRelAngleA",      &g_ads.fRelAngleA,      "fRelAngleA/F");
    tree->Branch("fRelAngleB",      &g_ads.fRelAngleB,      "fRelAngleB/F");

    tree->Branch("fChiA",           &g_ads.fChiA[0],        "fChiA/F");
    tree->Branch("fChiB",           &g_ads.fChiB[0],        "fChiB/F");
    tree->Branch("fChiC",           &g_ads.fChiA[1],        "fChiC/F");
    tree->Branch("fChiD",           &g_ads.fChiB[1],        "fChiD/F");
    tree->Branch("fMetaMatchQA",    &g_ads.fMetaMatchQA[0], "fMetaMatchQA/F");
    tree->Branch("fMetaMatchQB",    &g_ads.fMetaMatchQB[0], "fMetaMatchQB/F");
    tree->Branch("fMetaMatchQC",    &g_ads.fMetaMatchQA[1], "fMetaMatchQC/F");
    tree->Branch("fMetaMatchQD",    &g_ads.fMetaMatchQB[1], "fMetaMatchQD/F");
    tree->Branch("fVertDistA",      &g_ads.fVertDistA[0],   "fVertDistA/F");
    tree->Branch("fVertDistB",      &g_ads.fVertDistB[0],   "fVertDistB/F");
    tree->Branch("fVertDistC",      &g_ads.fVertDistA[1],   "fVertDistC/F");
    tree->Branch("fVertDistD",      &g_ads.fVertDistB[1],   "fVertDistD/F");
    tree->Branch("fpartB_tof",      &g_ads.fpartB_tof[0],   "fpartB_tof/F");
    tree->Branch("fpartB_tof_pid",  &g_ads.fpartB_tof_pid[0],"fpartB_tof_pid/I");
    tree->Branch("fpartD_tof",      &g_ads.fpartB_tof[1],   "fpartD_tof/F");
    tree->Branch("fpartD_tof_pid",  &g_ads.fpartB_tof_pid[1],"fpartD_tof_pid/I");
    tree->Branch("fHadesTracksNum", &g_ads.fHadesTracksNum, "fHadesTracksNum/I");
    tree->Branch("fFwDetTracksNum", &g_ads.fFwDetTracksNum, "fFwDetTracksNum/I");
    tree->Branch("fIsFwDetData1",   &g_ads.fIsFwDetData[0], "fIsFwDetData1/I");
    tree->Branch("fIsFwDetData2",   &g_ads.fIsFwDetData[1], "fIsFwDetData2/I");

    tree->Branch("fMultA",          &g_ads.fMultA[0],       "fMultA/I");
    tree->Branch("fMultB",          &g_ads.fMultB[0],       "fMultB/I");
    tree->Branch("fMultC",          &g_ads.fMultA[1],       "fMultC/I");
    tree->Branch("fMultD",          &g_ads.fMultB[1],       "fMultD/I");

    tree->Branch("fEventLCounter",  &g_ads.fEventLCounter,  "fEventLCounter/I");

    if (analysisType == KT::Sim)
    {
        tree->Branch("fRealLambda1",&g_ads.fRealLambda[0],  "fRealLambda1/I");
        tree->Branch("fRealLambda2",&g_ads.fRealLambda[1],  "fRealLambda2/I");
        tree->Branch("fPrimLambda1",&g_ads.fPrimLambda[0],  "fPrimLambda1/I");
        tree->Branch("fPrimLambda2",&g_ads.fPrimLambda[1],  "fPrimLambda2/I");
        tree->Branch("fCreationMech1",  &g_ads.fCreationMech[0],    "fCreationMech/I");
        tree->Branch("fCreationMech2",  &g_ads.fCreationMech[1],    "fCreationMech/I");
        tree->Branch("fDecayMech1", &g_ads.fDecayMech[0],   "fDecayMech/I");
        tree->Branch("fDecayMech2", &g_ads.fDecayMech[1],   "fDecayMech/I");
        tree->Branch("fGeantInfoNum",   &g_ads.fGeantInfoNum,   "fGeantInfoNum/I");
        tree->Branch("fGeantWeight",    &g_ads.fGeantWeight,"fGeantWeight/F");
    }

    tree->Branch("fGeaP",           &g_ads.fGeaP,           "fGeaP/F");
    tree->Branch("fGeaPx",          &g_ads.fGeaPx,          "fGeaPx/F");
    tree->Branch("fGeaPy",          &g_ads.fGeaPy,          "fGeaPy/F");
    tree->Branch("fGeaPz",          &g_ads.fGeaPz,          "fGeaPz/F");
    tree->Branch("fGeaTheta",       &g_ads.fGeaTheta,       "fGeaTheta/F");
    tree->Branch("fGeaPhi",         &g_ads.fGeaPhi,         "fGeaPhi/F");
    tree->Branch("fGeaXf",          &g_ads.fGeaXf,          "fGeaXf/F");
    tree->Branch("fGeaAngleAB",     &g_ads.fGeaAngleAB,     "fGeaAngleAB/F");

    tree->Branch("fPVtype",         &g_ads.fPVtype,         "fPVtype/I");

#ifdef SHOWREJECTED
    tree->Branch("fRejected",       &g_ads.fRejected,       "fRejected/I");
#endif /*SHOWREJECTED*/

    tree->Branch("fSortOrderMtd",   &g_ads.fSortOrderMtd,   "fSortOrderMtd/I");
}

void ef_dilambda_pp45::configureGraphicalCuts(KTrackInspector & trackInsp)
{
    const TString jsieben_pNb_cuts = "/scratch/e12f/knucl/jsieben/pNb/Cuts/";
    const TString aschmah_pp35_cuts = "/scratch/e12f/schmah/GraphicalCuts/pp35/";
    const TString jchen_pp35_cuts = "/home/gu27buz/hadesdst/pp35/";
    const TString jchen_pp35_cuts_sim = "/scratch/e12l/knucl/hades/jchen/pp35/GraphicalCuts/Sim/";
    const TString jchen_pp35_cuts_sim2 = "/scratch/e12f/knucl/rlalik/pp35/LambdaAnalysis/graph_cuts/";//"/scratch/e12l/knucl/hades/jchen/pp35/new_backup/GraphicalCuts/Sim/";

//    setChargePID(kTRUE);

    const TString rlalik_cuts = "/scratch/e12m/knucl/rlalik/pp35/LambdaAnalysis/Exp/";
    if (analysisType == KT::Sim)
    {
    // protons
//         trackInsp.registerCut(KT::MDC, KT::cut_p, jchen_pp35_cuts_sim2 + "Modified_PID_Cuts_Poly5_ChiiV5_Meth2.root", "Mdc_dEdx_P_cut_mod_ChiiV1_Sim_mod");
    // pions-
//         trackInsp.registerCut(KT::MDC, KT::cut_pim, jchen_pp35_cuts_sim2 + "Modified_PID_Cuts_Poly5_ChiiV5_Meth2.root", "Mdc_dEdx_PiP_cut_PID_mod_ChiiV2_Sim_mod_PiM");

//         trackInsp.registerdEdxPlot(KT::TOF);
//         trackInsp.registerdEdxPlot(KT::TOFINO);
    }
    else if (analysisType == KT::Exp)
    {
    // protons
//         trackInsp.registerCut(KT::MDC, KT::cut_p, jchen_pp35_cuts + "Mdc_dEdx_P_cut_mod_ChiiV1.root", "Mdc_dEdx_P_cut_mod_ChiiV1");
    // pions-
//         trackInsp.registerCut(KT::MDC, KT::cut_pim, jchen_pp35_cuts + "Mdc_dEdx_PiP_cut_PID_mod_ChiiV2.root", "Mdc_dEdx_PiP_cut_PID_mod_ChiiV2", kFALSE);    // new cat is already for pim
    }

    trackInsp.configureMetaSystem(KT::cut_p, KT::MDC);
    trackInsp.configureMetaSystem(KT::cut_pim, KT::MDC);
}

void ef_dilambda_pp45::initAnalysis(KT::Experiment exp, KT::AnalysisType analysisType)
{
    KAbstractAnalysis::initAnalysis(exp, analysisType);

    refBeamVector = getTargetGeomVector();
    beamCal = new KBeamCalibration(getTargetGeometry());
//     beamCal->initBeamCorrArray(analysisType);

    std::cout << "++ Analysis configuration" << std::endl;
    std::cout << "    nosecvertcuts    : " << flag_nosecvertcuts << std::endl;
    std::cout << "    elosscorr        : " << flag_elosscorr << std::endl;
    std::cout << "    no-sigmas        : " << flag_nosigmas << std::endl;
    std::cout << "    use-event-vertex : " << flag_useeventvertex << std::endl;
    std::cout << "    use-wall         : " << flag_usewall << std::endl;
    std::cout << "    Mtd              : " << par_Mtd << std::endl;
    std::cout << "    VertexDistX      : " << par_VertDistX << std::endl;
    std::cout << "    VertexDistA      : " << par_VertDistA << std::endl;
    std::cout << "    VertexDistB      : " << par_VertDistB << std::endl;
}

void ef_dilambda_pp45::finalizeAnalysis()
{
    delete beamCal;
    KAbstractAnalysis::finalizeAnalysis();
}

void ef_dilambda_pp45::ConfigureOptions(int& n, option* & long_options)
{
    static struct option _long_options[] =
    {
        /* These options set a flag. */
        { "nosecvercuts",       no_argument,        &flag_nosecvertcuts,    1 },
        { "elosscorr",          no_argument,        &flag_elosscorr,        1 },
        { "no-sigmas",          no_argument,        &flag_nosigmas,         1 },
        { "use-event-vertex",   no_argument,        &flag_useeventvertex,   1 },
        { "use-wall",           no_argument,        &flag_usewall,          1 },
        { "refit-fwdet",        no_argument,        &flag_refit_fwdet,      1 },
        /* These options don't set a flag.
        We distingu*ish them by their indices. */
//            {"events",     no_argument,       0, 'e'},
        { "Mtd",                required_argument,  0, 'm'},
        { "VertDistX",          required_argument,  0, 'x'},
        { "VertDistA",          required_argument,  0, 'p'},
        { "VertDistB",          required_argument,  0, 'q'},
    };

    long_options = _long_options;
    n = sizeof(_long_options)/sizeof(option);
}


int ef_dilambda_pp45::Configure(int c, const char * optarg)
{
    switch (c) {
        case 'm':
            par_Mtd = atol(optarg);
            break;
        case 'x':
            par_VertDistX = atol(optarg);
            break;
        case 'p':
            par_VertDistA = atol(optarg);
            break;
        case 'q':
            par_VertDistB = atol(optarg);
            break;

        default:
            abort();
            break;
    }

    return c;
}

void ef_dilambda_pp45::Usage() const
{
    std::cout <<
    "Analysis options: \n" <<
    "      --nosecvercuts\t\t\t - disable secondary vertex cuts\n" <<
    "\n\n";
}

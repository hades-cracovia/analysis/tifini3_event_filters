/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Rafał Lalik <rafal.lalik@@ph.tum.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef PP35_S1385P_PPIPPIM_H
#define PP35_S1385P_PPIPPIM_H

#include <TTree.h>

#include "event_filter_config.h"

#ifdef HAS_HADESDEDXEQUALIZER
#include "hades_dEdx_reader.h"
#endif

#include "henergylosscorrpar.h"

#include "Tifini3Config.h"

#include "KAbstractAnalysis.h"
#include "KVertex.h"
#include "KVector.h"
#include "KTrack.h"
#include "KTrackReconstructor.h"

class KBeamCalibrationApr07;

// constants
static const Float_t NULL_DATA = -10000.;
static const Float_t MASS_OK = 10000.;
static const Float_t ERR_NOT_IN_VERTEX = -10001.;
static const Float_t ERR_MASS_OUT_OF_RANGE = -10002.;
static const Float_t ERR_NO_LAMBDA = -10003.;
static const Float_t ERR_VERTEX_Z_MISSMATCH = -10004.;
static const Float_t ERR_SIM_VERTEX_MISSMATCH = -10005.;
static const Float_t ERR_BAD_MMQ = -10006.;

//#define SHOWREJECTED

struct AnaDataSet
{
    // Kinematic properties
    Float_t fLambda_MM;
    Float_t fS1385p_MM;
    Float_t fMt;

    // track-reco properties
    Float_t fLambda_MTD;
    Float_t fS1385p_MTD;
    Float_t fVertDistX;
    Float_t fLambda_PVA;
//     Float_t fS1385p_PVA;

    // vertex properties
    Float_t fEventVertexX;
    Float_t fEventVertexY;
    Float_t fEventVertexZ;

    Float_t fDVres;

    // daughter properties
    Float_t fMomAx, fMomAy, fMomAz, fMomBx, fMomBy, fMomBz, fMomCx, fMomCy, fMomCz;
    Float_t fAngleAB;
    Float_t fRelAngleA, fRelAngleB;
    Float_t fChiA, fChiB, fChiC;
    Float_t fMetaMatchQA, fMetaMatchQB, fMetaMatchQC;
    Float_t fVertDistA, fVertDistB;
    Float_t fVertDistL, fVertDistC;
    Int_t fMultA, fMultB;

    // event stats
    Int_t fHadesTracksNum;
    Int_t fEventLCounter;
    Int_t fRealLambda;
    Int_t fPrimLambda;
    Int_t fRealS1385p;
    Int_t fPrimS1385p;

    // other
    Int_t fGeantInfoNum;
    Float_t fGeantWeight;

    Float_t ret;
    Int_t fSortOrderMtd;
#ifdef SHOWREJECTED
    Int_t fRejected;
#endif

    // Wal data
    Float_t fWallT;
    Float_t fWallX;
    Float_t fWallY;
    Float_t fWallR;
    Float_t fWallCharge;
    Float_t fWallP;
    Float_t fWallPx;
    Float_t fWallPy;
    Float_t fWallPz;
    Float_t fWallBeta;
    Float_t fWallGamma;
    Int_t fWallClusterSize;
    Int_t fWallClustersNum;

    // Geant
    Float_t fGeaP, fGeaPx, fGeaPy, fGeaPz;
    Float_t fGeaTheta, fGeaPhi;
    Float_t fGeaAngleAB;
    Float_t fGeaP_A, fGeaP_B, fGeaP_C;

    Int_t fPVtype;

    KTrack tr_lambda_cms;
    KTrack tr_lambda_a;
    KTrack tr_lambda_a_cms;
    KTrack tr_lambda_b;
    KTrack tr_lambda_b_cms;
    KTrack tr_s1385p_cms;
    KTrack tr_s1385p_c;
    KTrack tr_s1385p_c_cms;

    KVertex vx_lambda;
    KVertex vx_s1385p;

    KTrackReconstructor trec_lambda;
    KTrackReconstructor trec_s1385p;

    void clear()
    {
        fHadesTracksNum = 0;

        fGeantWeight = 1.0;
        fEventVertexX = 0.0;
        fEventVertexY = 0.0;
        fEventVertexZ = 0.0;

        fEventLCounter = 0;

        fMultA = 0;
        fMultB = 0;
    }

    void init()
    {
        fLambda_MM = NULL_DATA;
        fS1385p_MM = NULL_DATA;
        fMt = NULL_DATA;

        fLambda_MTD = NULL_DATA;
        fS1385p_MTD = NULL_DATA;
        fVertDistX = NULL_DATA;
        fLambda_PVA = NULL_DATA;
//         fS1385p_PVA = NULL_DATA;

        fDVres = NULL_DATA;

        fMomAx = fMomAy = fMomAz = fMomBx = fMomBy = fMomBz = NULL_DATA;

        fAngleAB = NULL_DATA;
        fRelAngleA = fRelAngleB = NULL_DATA;
        fChiA = fChiB = NULL_DATA;
        fMetaMatchQA = fMetaMatchQB = NULL_DATA;
        fVertDistA = fVertDistB = NULL_DATA;

        fGeantInfoNum = 0;

        fWallT = 0.0;
        fWallX = 0.0;
        fWallY = 0.0;
        fWallR = 0.0;
        fWallCharge = 0.0;
        fWallP = 0.0;
        fWallPx = 0.0;
        fWallPy = 0.0;
        fWallPz = 0.0;
        fWallBeta = 0.0;
        fWallGamma = 0.0;
        fWallClusterSize = 0;
        fWallClustersNum = 0;

        fGeaP = fGeaPx = fGeaPy = fGeaPz = NULL_DATA;
        fGeaTheta = fGeaPhi = NULL_DATA;
        fGeaAngleAB = NULL_DATA;
        fGeaP_A = fGeaP_B = fGeaP_C = NULL_DATA;

        fPVtype = NULL_DATA;

#ifdef SHOWREJECTED
        fRejected = 0;
#endif /*SHOWREJECTED*/

        ret = 0.;
        fSortOrderMtd = 0;
    }
};

class ef_s1385p_ppippim_pp35 : public KAbstractAnalysis
{
public:
    ef_s1385p_ppippim_pp35(const TString & analysisName, const TString & treeName);
    virtual ~ef_s1385p_ppippim_pp35();

    virtual bool analysis(HEvent * event, Int_t event_num, Int_t run_id);

    void initAnalysis(KT::Experiment exp, KT::AnalysisType analysisType);
    void finalizeAnalysis();

    void ConfigureOptions(int & n, option * & long_options);
    int Configure(int c, const char * optarg);
    void Usage() const;

protected:
    void configureTree(TTree * tree);
    void configureGraphicalCuts(KTrackInspector & cuts);

    AnaDataSet singlePairAnalysis(HEvent * event, Int_t run_id, UInt_t pid_a, UInt_t pid_b, int trackA_num, int trackB_num, bool quick_run = false);
    AnaDataSet singleHadesPairAnalysisS1385p(HEvent * fEvent, Int_t event_num, const AnaDataSet & ads_a, UInt_t pid_b, int trackB_num, bool quick_run = false);

    HEnergyLossCorrPar * eLossCorr;
    KBeamCalibrationApr07 * beamCal;
    HGeomVector refBeamVector;
    HGeomVector beamVector;
    TLorentzVector Vec_pp35_sum;

    // opts
    int flag_nosecvertcuts;
    int flag_elosscorr;
    int flag_nosigmas;
    int flag_useeventvertex;
    int flag_usewall;

    int par_Mtd;			// CLI
    int par_VertDistX;		// CLI
    int par_VertDistA;		// CLI
    int par_VertDistB;		// CLI

    KTrack track_lambda_cms;
    KTrack track_lambda_a;
    KTrack track_lambda_a_cms;
    KTrack track_lambda_b;
    KTrack track_lambda_b_cms;
    KTrack track_s1385p_cms;
    KTrack track_s1385p_c;
    KTrack track_s1385p_c_cms;
    KVertex vertex_lambda;
    KVertex vertex_s1385p;

    KTrackReconstructor trec_lambda;
    KTrackReconstructor trec_s1385p;

    std::map<Float_t, Long_t> cut_counters;
};

#endif // PP35_S1385P_PPIPPIM_H

/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Rafał Lalik <rafal.lalik@ph.tum.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <getopt.h>

#include <hphysicsconstants.h>
#include <hparticlecandsim.h>
#include <hparticlecand.h>
#include <hparticletool.h>
#include <hforwardcand.h>
#include <hfrpchit.h>
#include <hforwardtools.h>
#include <frpcdef.h>

#include "KTools.h"
#include "KTrackInspector.h"
#include "KBeamCalibration.h"
#include "KCutInside.h"

#include "ef_xim_pp45.h"

#ifdef SHOWREJECTED
//int fRejected;
#endif /*SHOWREJECTED*/

static const float E_kin_beam       = 4500.0;
static const float E_kin_target     = 0.0;
static const float E_total_beam     = E_kin_beam + HPhysicsConstants::mass(14);
static const float E_total_target   = E_kin_target + HPhysicsConstants::mass(14);
static const float pz_beam          = sqrt(E_total_beam*E_total_beam-HPhysicsConstants::mass(14)*HPhysicsConstants::mass(14));

static const float LAMBDA_MASS = 1115.9;
static const float XI_MASS = 1321.71;

// static const float D2R = TMath::DegToRad();
// static const float R2D = TMath::RadToDeg();

static AnaDataSet g_ads, g_adsXi;

float calcAngleVar(HGeomVector & v1, HGeomVector & v2)
{
    TVector3 _v1; _v1.SetXYZ(v1.X(), v1.Y(), v1.Z());
    TVector3 _v2; _v2.SetXYZ(v2.X(), v2.Y(), v2.Z());

    return _v1.Angle(_v2);
}

float calcAngleVar(HGeomVector & v1, TLorentzVector & v2)
{
    TVector3 _v1; _v1.SetXYZ(v1.X(), v1.Y(), v1.Z());
    TVector3 _v2; _v2.SetXYZ(v2.Px(), v2.Py(), v2.Pz());

    return _v1.Angle(_v2);
}

TVector3 toTV3(const HGeomVector & v) {
    TVector3 n;
    n.SetXYZ(v.getX(), v.getY(), v.getZ());
    return n;
}

void POCA(const TVector3 & mc, const TVector3 & b, const TVector3 & d,
          float & poca_t, float & poca_l, float & poca, float & poca_x, float & poca_y) {


    // plane normal vector
//     static const TVector3 mc_plane_n_xy(0, 0, 1);
//
//     // find track and plane corssing
//     Float_t dd = ((mc - b) * mc_plane_n_xy)/(d * mc_plane_n_xy);
//
//     TVector3 xy_vec = b + dd*d;
//
//     // find POCA in transverse
//     poca_t = (mc-xy_vec).Mag();

    // find POCA general
    TVector3 _temp1 = b - mc;
    TVector3 d_v_mc = _temp1 - (_temp1*d)*d;

    poca = d_v_mc.Mag();

    poca_l = d_v_mc.Z();
    d_v_mc.SetZ(0);

    poca_t = d_v_mc.Mag();

    poca_x = d_v_mc.X();
    poca_y = d_v_mc.Y();

//     // find plane which is along z axis, rotated with normal to vector by POCA
//     TVector3 mc_plane_n_z(d_v_mc.X(), d_v_mc.Y(), 0);   // plane normal
//     Float_t ddz = ((mc - b) * mc_plane_n_z)/(d * mc_plane_n_z);
//     TVector3 z_vec = b + ddz*d;
//
//     poca_l = (mc-z_vec).Mag();

#define SHOW_POKA 0
#if SHOW_POKA
    printf("G: v= %f,%f,%f  d= %f  XY: v= %f,%f,%f  d=%f   Z: d=%f \n",
           d_v_mc.X(), d_v_mc.Y(), d_v_mc.Z(), poca,
           (mc-xy_vec).X(), (mc-xy_vec).Y(), (mc-xy_vec).Z(), poca_t,
           poca_l
          );
#endif
}

ef_xim_pp45::ef_xim_pp45(const TString& analysisName, const TString& treeName)
    : KAbstractAnalysis(analysisName, treeName)
    , flag_nosecvertcuts(0), flag_elosscorr(0), flag_nosigmas(0)
    , flag_useeventvertex(0), flag_usewall(0), flag_refit_fwdet(0)
    , par_Mtd(10), par_VertDistX(45), par_VertDistA(5), par_VertDistB(15)
{
//     setGoodEventSelector(Particle::kGoodTRIGGER |
//         Particle::kGoodVertexClust |
// //         Particle::kGoodVertexCand |
//         Particle::kGoodSTART |
//         Particle::kNoPileUpSTART);

    setExperimentType(KT::pp45);
    setGoodEventSelector(0);

    setPidSelectionHades(KT::p, KT::Beta | KT::Charge);
    setPidSelectionHades(KT::pim, KT::Beta | KT::Charge);

    eLossCorr = new HEnergyLossCorrPar("eLossCorr", "eLossCorr", "eLossCorr");
    eLossCorr->setDefaultPar("jan04");
}

bool ef_xim_pp45::analysis(HEvent * fEvent, Int_t event_num, Int_t /*run_id*/)
{
//     printf("%d  %d\n", cand_size, vect_size);
    if ( (hades_tracks.size() + forward_tracks.size()) < 2 )
        return false;

    int A_PID = KT::p;    // proton
    int B_PID = KT::pim;    // pi-

    g_ads.fHadesTracksNum = hades_tracks.size();
    g_ads.fFwDetTracksNum = forward_tracks.size();
    g_ads.fIsFwDetData = 0;

    g_ads.fGeantWeight = 0;

    g_ads.fEventVertexX = fEvent->getHeader()->getVertexCluster().getX();
    g_ads.fEventVertexY = fEvent->getHeader()->getVertexCluster().getY();
    g_ads.fEventVertexZ = fEvent->getHeader()->getVertexCluster().getZ();

    g_ads.fEventLCounter = 0;

    g_ads.fMultA = 0;
    g_ads.fMultB = 0;

    g_ads.fIsFwDetData = 0;

    g_ads.fEventSeqNumber = fEvent->getHeader()->getEventSeqNumber();

    std::vector<AnaDataSet> ads_arr;
    ads_arr.reserve(10000);

    size_t combo_cnt = 0;

    beamVector = refBeamVector;
//     if (analysisType == KT::Exp)
//     {
//         beamVector = beamCal->calculateBeamOffset(event->getRunId());
//     } else {
        beamVector = refBeamVector;
//     }

    TVector3 p_beam_vec(0.0, 0.0, pz_beam);
    TLorentzVector Vec_pp45_beam    = TLorentzVector(p_beam_vec.X(), p_beam_vec.Y(), p_beam_vec.Z(), E_total_beam);
    TLorentzVector Vec_pp45_target  = TLorentzVector(0.0, 0.0, 0.0, E_total_target);
    Vec_pp45_sum       = Vec_pp45_beam + Vec_pp45_target;


    // loops on each combination: p(L)-pi(L)-pi(Xi),
    // where: p(L) and pi(L) comes from Lambda and pi(Xi) comes from XI
    // fields can be H(ades) or F(wDet)

    // H-
    for(int i = 0; i < g_ads.fHadesTracksNum; ++i)
    {
        // H-H-
        for(int j = 0; j < g_ads.fHadesTracksNum; ++j)
        {
            if (!(hades_tracks[i].pid[A_PID][KT::Beta] and hades_tracks[i].pid[A_PID][KT::Charge]))
                break;

            if (i == j)
                continue;

            if (!(hades_tracks[j].pid[B_PID][KT::Beta] and hades_tracks[j].pid[B_PID][KT::Charge]))
                continue;

            AnaDataSet ads_ret = singleHadesPairAnalysis(fEvent, event_num, A_PID, B_PID, i, j);
            ads_ret.fIsFwDetData = 0;

            if (ads_ret.ret == ERR_MASS_OUT_OF_RANGE)
                continue;

            if (ads_ret.ret <= NULL_DATA)
                continue;
            // H-H-H
            for(int k = 0; k < g_ads.fHadesTracksNum; ++k)
            {
                if (k == j or k == i)
                    continue;

                if (!(hades_tracks[k].pid[B_PID][KT::Beta] and hades_tracks[k].pid[B_PID][KT::Charge]))
                    continue;

                AnaDataSet ads_xi_ret = singleHadesPairAnalysisXi(fEvent, event_num, ads_ret, B_PID, k);
                ads_xi_ret.fIsFwDetDataXi = 0;

                if (ads_xi_ret.ret == ERR_MASS_OUT_OF_RANGE)
                    continue;

                if (ads_xi_ret.ret <= NULL_DATA)
                    continue;

                hades_tracks[i].is_used = true;
                hades_tracks[j].is_used = true;
                hades_tracks[k].is_used = true;

                ads_arr.push_back(ads_xi_ret);
                ++combo_cnt;
            }

            // H-H-F
            for(int k = 0; k < g_ads.fFwDetTracksNum; ++k)
            {
                AnaDataSet ads_xi_ret = singleFwDetPairAnalysisXi(fEvent, event_num, ads_ret, B_PID, k);
                ads_xi_ret.fIsFwDetDataXi = 1;

                if (ads_xi_ret.ret == ERR_MASS_OUT_OF_RANGE)
                    continue;

                if (ads_xi_ret.ret <= NULL_DATA)
                    continue;

                hades_tracks[i].is_used = true;
                hades_tracks[j].is_used = true;
                forward_tracks[k].is_used = true;

                ads_arr.push_back(ads_xi_ret);
                ++combo_cnt;
            }
        }

        for(int j = 0; j < /*fMultB*/g_ads.fFwDetTracksNum; ++j)
        {
            AnaDataSet ads_ret;

// No usefull data with assumption that proton is in HADES and pion in FwDet
//             // H-F
//             if (hades_tracks[i].pid[A_PID][KT::Beta] and hades_tracks[i].pid[A_PID][KT::Charge])
//             {
//                 ads_ret = singleFwDetPairAnalysis(fEvent, event_num, A_PID, B_PID, i, j);
//                 ads_ret.fIsFwDetData = 2;
//
//                 if (ads_ret.ret == ERR_MASS_OUT_OF_RANGE)
//                     continue;
//
//                 if (ads_ret.ret <= NULL_DATA)
//                     continue;
//             }
//             else
            // F-H
            if (hades_tracks[i].pid[B_PID][KT::Beta] and hades_tracks[i].pid[B_PID][KT::Charge])
            {
                ads_ret = singleFwDetPairAnalysis(fEvent, event_num, B_PID, A_PID, i, j);
                ads_ret.fIsFwDetData = 1;

                if (ads_ret.ret == ERR_MASS_OUT_OF_RANGE)
                    continue;

                if (ads_ret.ret <= NULL_DATA)
                    continue;
            }
            else
            {
                continue;
            }

            // ?-?-H
            for(int k = 0; k < g_ads.fHadesTracksNum; ++k)
            {
                if (k == i)
                    continue;

                if (!(hades_tracks[k].pid[B_PID][KT::Beta] and hades_tracks[k].pid[B_PID][KT::Charge]))
                    continue;

                AnaDataSet ads_xi_ret = singleHadesPairAnalysisXi(fEvent, event_num, ads_ret, B_PID, k);
                ads_xi_ret.fIsFwDetDataXi = 0;

                if (ads_xi_ret.ret == ERR_MASS_OUT_OF_RANGE)
                    continue;

                if (ads_xi_ret.ret <= NULL_DATA)
                    continue;

                hades_tracks[i].is_used = true;
                forward_tracks[j].is_used = true;
                hades_tracks[k].is_used = true;

                ads_arr.push_back(ads_xi_ret);
                ++combo_cnt;
            }

            // ?-?-F
            // This can be removed as well, only ~1% yield from this selection but huge background.
            for(int k = 0; k < g_ads.fFwDetTracksNum; ++k)
            {
                if (k == j)
                    continue;

                AnaDataSet ads_xi_ret = singleFwDetPairAnalysisXi(fEvent, event_num, ads_ret, B_PID, k);
                ads_xi_ret.fIsFwDetDataXi = 2;

                if (ads_xi_ret.ret == ERR_MASS_OUT_OF_RANGE)
                    continue;

                if (ads_xi_ret.ret <= NULL_DATA)
                    continue;

                hades_tracks[i].is_used = true;
                forward_tracks[j].is_used = true;
                forward_tracks[k].is_used = true;

                ads_arr.push_back(ads_xi_ret);
                ++combo_cnt;
            }
        }
    }

    for (size_t i = 0; i < combo_cnt; ++i)
    {
        g_ads = ads_arr[i];

        // copying
        track_lambda_cms = ads_arr[i].tr_lambda_cms;
        track_lambda_a = ads_arr[i].tr_lambda_a;
        track_lambda_b = ads_arr[i].tr_lambda_b;

        track_xim_cms = ads_arr[i].tr_xim_cms;
        track_xim_c = ads_arr[i].tr_xim_c;

        vertex_lambda = ads_arr[i].vx_lambda;
        vertex_xim = ads_arr[i].vx_xim;

        vertex_primary = ads_arr[i].vx_primary;

        trec_lambda = ads_arr[i].trec_lambda;
        trec_xim = ads_arr[i].trec_xim;

        // filling
        track_lambda_cms.fill();
        track_lambda_a.fill();
        track_lambda_b.fill();

        track_xim_cms.fill();
        track_xim_c.fill();

        vertex_lambda.fill();
        vertex_xim.fill();
        vertex_primary.fill();

        trec_lambda.fill();
        trec_xim.fill();

        getTree()->Fill();
    }

    return true;
}

AnaDataSet ef_xim_pp45::singleHadesPairAnalysis(HEvent * /*fEvent*/, int /*event_num*/, UInt_t pid_a, UInt_t pid_b, int trackA_num, int trackB_num, bool quick_run)
{
    AnaDataSet ads = g_ads;
    ads.init();
//     ads.fGeantWeight = pcand->getGeantGenweight(); FIXME

    HGeomVector dirMother;

    TObject * o_a = hades_tracks[trackA_num].cand;
    TObject * o_b = hades_tracks[trackB_num].cand;

    HParticleCand trackA = *(HParticleCand*)o_a;
    HParticleCand trackB = *(HParticleCand*)o_b;

    trackA.calc4vectorProperties(HPhysicsConstants::mass(pid_a));
    trackB.calc4vectorProperties(HPhysicsConstants::mass(pid_b));

    Float_t fMomA = trackA.getMomentum();
    Float_t fMomB = trackB.getMomentum();

    if (flag_elosscorr and analysisType == KT::Exp)
    {
        float momentum_A_corr = eLossCorr->getCorrMom(pid_a, fMomA, trackA.getTheta());
        float momentum_B_corr = eLossCorr->getCorrMom(pid_b, fMomB, trackB.getTheta());
        trackA.setMomentum(momentum_A_corr);
        trackB.setMomentum(momentum_B_corr);
    }

    trackA.calc4vectorProperties(HPhysicsConstants::mass(pid_a));
    trackB.calc4vectorProperties(HPhysicsConstants::mass(pid_b));

    ads.tr_lambda_a = trackA;
    ads.tr_lambda_b = trackB;
    ads.trec_lambda.reconstruct(trackA, trackB);
    ads.fLambda_MTD = ads.trec_lambda.getMTD();        // minimum distance between the two tracks

    // I do not need so many data!
    if (ads.trec_lambda.M() > 1200)
    {
    #ifdef SHOWREJECTED
        ads.fRejected = ERR_MASS_OUT_OF_RANGE;
    #else
        ads.ret = ERR_MASS_OUT_OF_RANGE;
        return ads;
    #endif /*SHOWREJECTED*/
    }

    if (quick_run)
    {
        ads.ret = (LAMBDA_MASS - ads.trec_lambda.M());
        return ads;// * fMinTrackDist;
    }

    // extra checks for the simulation analysis
    HVirtualCandSim * tcs_a = dynamic_cast<HVirtualCandSim*>(o_a);
    HVirtualCandSim * tcs_b = dynamic_cast<HVirtualCandSim*>(o_b);

    if (analysisType == KT::Sim && tcs_a && tcs_b)
    {
        int GeantPIDA = tcs_a->getGeantPID();
        int GeantPIDB = tcs_b->getGeantPID();

        int GeantPIDAparent = tcs_a->getGeantParentPID();
        int GeantPIDBparent = tcs_b->getGeantParentPID();

        int GeantPIDBGparent = tcs_b->getGeantGrandParentPID();

        ads.fRealLambda = (
            // proton-pim pair
            (GeantPIDA == 14 and GeantPIDAparent == 18 and GeantPIDB == 9 and GeantPIDBparent == 18)
            or
            // proton-pi- (decaying into mu-)
            (GeantPIDA == 14 and GeantPIDAparent == 18 and GeantPIDB == 6 and GeantPIDBparent == 9 and GeantPIDBGparent == 18)
        );
        ads.fGeaPiMuonLa = (GeantPIDB == 6 and GeantPIDBparent == 9 and GeantPIDBGparent == 18);
    }
    else
    {
        ads.fRealLambda = kFALSE;
    }

    ads.fChiA = trackA.getChi2();
    ads.fChiB = trackB.getChi2();

    ads.fMetaMatchQA = trackA.getMetaMatchQuality();
    ads.fMetaMatchQB = trackB.getMetaMatchQuality();

    dirMother.setXYZ(ads.trec_lambda.X(), ads.trec_lambda.Y(), ads.trec_lambda.Z());    // direction vector of the mother particle
    ads.vx_lambda = ads.trec_lambda.getDecayVertex();

    if (analysisType == KT::Sim && tcs_a && tcs_b)
    {
        ads.fGeaVxLa = tcs_a->getGeantxVertex();
        ads.fGeaVyLa = tcs_a->getGeantyVertex();
        ads.fGeaVzLa = tcs_a->getGeantzVertex();

        TVector3 dv(ads.vx_lambda.X() - ads.fGeaVxLa,
                    ads.vx_lambda.Y() - ads.fGeaVyLa,
                    ads.vx_lambda.Z() - ads.fGeaVzLa);

        ads.fVMCD_La = dv.Mag();
        ads.fVMCD_La_x = dv.X();
        ads.fVMCD_La_y = dv.Y();
        ads.fVMCD_La_z = dv.Z();

        Float_t poca_t, poca_l, poca, poca_x, poca_y;
        TVector3 _b, _d;
        HGeomVector _bg, _dg;
        HParticleTool::calcSegVector(trackA.getZ(), trackA.getR(),
                               TMath::DegToRad()*trackA.getPhi(),
                               TMath::DegToRad()*trackA.getTheta(),
                               _bg, _dg);

        _b.SetXYZ(_bg.X(), _bg.Y(), _bg.Z());
        _d.SetXYZ(_dg.X(), _dg.Y(), _dg.Z());
        TVector3 mc(ads.fGeaVxLa, ads.fGeaVyLa, ads.fGeaVzLa);
        POCA(mc, _b, _d, poca_t, poca_l, poca, poca_x, poca_y);

        ads.fLaPres = poca;
        ads.fLaPres_t = poca_t;
        ads.fLaPres_l = poca_l;
        ads.fLaPres_x = poca_x;
        ads.fLaPres_y = poca_y;
    }

    ads.fVertDistA = ads.trec_lambda.getMTDa();
    ads.fVertDistB = ads.trec_lambda.getMTDb();

//     if ( !(ads.vx_primary.getR() < 10.0 and ads.vx_primary.getZ() < 0.0 and ads.vx_primary.getZ() > -90.0) )
//     {
// #ifdef SHOWREJECTED
//         ads.fRejected = ERR_NOT_IN_VERTEX;
// #else
//         ads.ret = ERR_NOT_IN_VERTEX;
//         return ads;
// #endif /*SHOWREJECTED*/
//     }

    ads.fAngleAB    = trackA.Angle(trackB.Vect());
    ads.fRelAngleA  = ads.trec_lambda.Angle(trackA.Vect());
    ads.fRelAngleB  = ads.trec_lambda.Angle(trackB.Vect());

    //Boost in CMS: ***********************************************
    TLorentzVector trackAB_CMS = ads.trec_lambda;

    trackAB_CMS.Boost(-Vec_pp45_sum.BoostVector());
    ads.tr_lambda_cms = trackAB_CMS;

    TLorentzVector trackA_CMS = trackA;
    trackA_CMS.Boost(-Vec_pp45_sum.BoostVector());
    ads.fMomA_cms            = trackA_CMS.P();

    TLorentzVector trackB_CMS = trackB;
    trackB_CMS.Boost(-Vec_pp45_sum.BoostVector());
    ads.fMomB_cms            = trackB_CMS.P();

    //*************************************************************

    ads.fMt                = ads.trec_lambda.Mt();               // Transverse mass

    ads.fLambda_MM = (Vec_pp45_sum - ads.trec_lambda).M2();      // Missing Mass

    ++ads.fEventLCounter;

    ads.ret = ads.trec_lambda.M();
    return ads;
}

AnaDataSet ef_xim_pp45::singleHadesPairAnalysisXi(HEvent* /*fEvent*/, Int_t /*event_num*/, const AnaDataSet& ads_a, UInt_t pid_b, int trackB_num, bool /*quick_run*/)
{
    AnaDataSet ads = ads_a;

    HGeomVector dirMother, PrimVertexMother;

    TObject * o_b = hades_tracks[trackB_num].cand;

    KTrackReconstructor trackA = ads_a.trec_lambda;
    HParticleCand trackB = *(HParticleCand*)o_b;

    trackB.calc4vectorProperties(HPhysicsConstants::mass(pid_b));

    Float_t fMomB = trackB.getMomentum();

    if (flag_elosscorr and analysisType == KT::Exp)
    {
        float momentum_B_corr = eLossCorr->getCorrMom(pid_b, fMomB, trackB.getTheta());
        trackB.setMomentum(momentum_B_corr);
    }

    trackB.calc4vectorProperties(HPhysicsConstants::mass(pid_b));
    ads.tr_xim_c = trackB;

    ads.trec_xim.reconstruct(trackA, trackB);
    ads.vx_xim = ads.trec_xim.getDecayVertex();
    ads.fXim_MTD = ads.trec_xim.getMTD();

    // distance between Xi- and Lambda
    HGeomVector v1 = ads.vx_lambda - ads.vx_xim;
    ads.fVertDistX = (v1).length();
    ads.fLambda_PVA = calcAngleVar(v1, ads.trec_lambda);

    // cms system
    TLorentzVector trackLC_CMS = ads.trec_xim;
    trackLC_CMS.Boost(-Vec_pp45_sum.BoostVector());
    ads.tr_xim_cms = trackLC_CMS;

    TLorentzVector trackC_CMS = trackB;
    trackC_CMS.Boost(-Vec_pp45_sum.BoostVector());
    ads.fMomC_cms            = trackC_CMS.P();

    // I do not need so many data!
    KCutInside<Float_t> xi_mass_test(XI_MASS - 100.0, XI_MASS + 100.0, KT::WEAK, KT::WEAK);

    if (!xi_mass_test.test(ads.trec_xim.M()))
    {
    #ifdef SHOWREJECTED
        ads.fRejected = ERR_MASS_OUT_OF_RANGE;
    #else
        ads.ret = ERR_MASS_OUT_OF_RANGE;
        return ads;
    #endif /*SHOWREJECTED*/
    }

    // extra checks for the simulation analysis
    HVirtualCandSim * tcs_b = dynamic_cast<HVirtualCandSim*>(o_b);

    if (analysisType == KT::Sim && tcs_b)
    {
        int GeantPIDB = tcs_b->getGeantPID();
        int GeantPIDBparent = tcs_b->getGeantParentPID();
        int GeantPIDBGparent = tcs_b->getGeantGrandParentPID();

        ads.fRealXim = (
            // proton-pim pair
            (ads.fRealLambda and GeantPIDB == 9 and GeantPIDBparent == 23)
            or
            // proton-pi- (decaying into mu-)
            (ads.fRealLambda and GeantPIDB == 6 and GeantPIDBparent == 9 and GeantPIDBGparent == 23)
        );

        ads.fGeaPiMuonXim = (GeantPIDB == 6 and GeantPIDBparent == 9 and GeantPIDBGparent == 23);

        ads.fGeaVxXim = tcs_b->getGeantxVertex();
        ads.fGeaVyXim = tcs_b->getGeantyVertex();
        ads.fGeaVzXim = tcs_b->getGeantzVertex();

        TVector3 dv(ads.vx_xim.X() - ads.fGeaVxXim,
                    ads.vx_xim.Y() - ads.fGeaVyXim,
                    ads.vx_xim.Z() - ads.fGeaVzXim);

        ads.fVMCD_Xim = dv.Mag();
        ads.fVMCD_Xim_x = dv.X();
        ads.fVMCD_Xim_y = dv.Y();
        ads.fVMCD_Xim_z = dv.Z();
    }

    ads.fXim_MM = (Vec_pp45_sum - ads.trec_xim).M2();        // Missing Mass

    ads.ret = ads.trec_xim.M();
    return ads;
}

AnaDataSet ef_xim_pp45::singleFwDetPairAnalysis(HEvent * /*fEvent*/, Int_t /*event_num*/, UInt_t pid_a, UInt_t pid_b, int trackA_num, int trackB_num, bool quick_run)
{
    AnaDataSet ads = g_ads;
    ads.init();
//     ads.fGeantWeight = pcand->getGeantGenweight(); FIXME

    HGeomVector dirMother;

    TObject * o_a = hades_tracks[trackA_num].cand;
    TObject * o_b = forward_tracks[trackB_num].cand;

    HParticleCand trackA = *(HParticleCand*)o_a;
    HForwardCand trackB = *(HForwardCand*)o_b;

    if (trackB.getTofRec() == 0)
    {
    #ifdef SHOWREJECTED
        ads.fRejected = ERR_NO_FWDET_TOF;
    #else
        ads.ret = ERR_NO_FWDET_TOF;
        return ads;
    #endif /*SHOWREJECTED*/
    }

    trackA.calc4vectorProperties(HPhysicsConstants::mass(pid_a));
    trackB.calc4vectorProperties(HPhysicsConstants::mass(pid_b));

    Float_t fMomA = trackA.getMomentum();
//     Float_t fMomB = trackB.getMomentum();

    if (flag_elosscorr and analysisType == KT::Exp)
    {
        float momentum_A_corr = eLossCorr->getCorrMom(pid_a, fMomA, trackA.getTheta());
//         float momentum_B_corr = eLossCorr->getCorrMom(pid_b, fMomB, trackB.getTheta());
        trackA.setMomentum(momentum_A_corr);
//         trackB.setMomentum(momentum_B_corr);
    }

    trackA.calc4vectorProperties(HPhysicsConstants::mass(pid_a));

    ads.tr_lambda_a = trackA;

    ads.trec_lambda.reconstruct(trackA, trackB);
    ads.vx_lambda = ads.trec_lambda.getDecayVertex();

    if (flag_refit_fwdet && trackB.getTofRec())
    {
        HCategory * pFRpcHit = gHades->getCurrentEvent()->getCategory(catFRpcHit);
        HFRpcHit * frpchit = dynamic_cast<HFRpcHit*>(pFRpcHit->getObject(trackB.getFRpcHitIndex()));
        HForwardTools::correctPathLength(&trackB, HGeomVector(ads.vx_lambda.X(), ads.vx_lambda.Y(), ads.vx_lambda.Z()), frpchit);
        HForwardTools::correctTrackProperties(&trackB, HPhysicsConstants::mass(14));

        ads.trec_lambda.reconstruct(trackA, trackB);
        ads.vx_lambda = ads.trec_lambda.getDecayVertex();
    }
    ads.tr_lambda_b = trackB;

    if (trackB.getTofRec() <= 0)
    {
    #ifdef SHOWREJECTED
        ads.fRejected = ERR_NO_FWDET_TOF;
    #else
        ads.ret = ERR_NO_FWDET_TOF;
        return ads;
    #endif /*SHOWREJECTED*/
    }

    ads.fpartB_tof = trackB.getTof();
    ads.fLambda_MTD = ads.trec_lambda.getMTD();        // minimum distance between the two tracks

    // we do not need so many data!
    if (ads.trec_lambda.M() > 1200)
    {
    #ifdef SHOWREJECTED
        ads.fRejected = ERR_MASS_OUT_OF_RANGE;
    #else
        ads.ret = ERR_MASS_OUT_OF_RANGE;
        return ads;
    #endif /*SHOWREJECTED*/
    }

    if (quick_run)
    {
        ads.ret = (LAMBDA_MASS - ads.trec_lambda.M());
        return ads;// * fMinTrackDist;
    }

    // extra checks for the simulation analysis
    HVirtualCandSim * tcs_a = dynamic_cast<HVirtualCandSim*>(o_a);
    HVirtualCandSim * tcs_b = dynamic_cast<HVirtualCandSim*>(o_b);

    if (analysisType == KT::Sim && tcs_a && tcs_b)
    {
        int GeantPIDA = tcs_a->getGeantPID();
        int GeantPIDB = tcs_b->getGeantPID();

        int GeantPIDAparent = tcs_a->getGeantParentPID();
        int GeantPIDBparent = tcs_b->getGeantParentPID();

        int GeantPIDAGparent = tcs_a->getGeantGrandParentPID();
        int GeantPIDBGparent = tcs_b->getGeantGrandParentPID();

        Bool_t A_is_OK = kFALSE;
        Bool_t B_is_OK = kFALSE;

        if (14 == pid_a)
        {
            A_is_OK = GeantPIDAparent == 18;
        }
        else if (9 == pid_a)
        {
            A_is_OK = (GeantPIDA == 9 and GeantPIDAparent == 18) or
                        (GeantPIDA == 6 and GeantPIDAparent == 9 and GeantPIDAGparent == 18);
            ads.fGeaPiMuonLa = (GeantPIDA == 6 and GeantPIDAparent == 9 and GeantPIDAGparent == 18);
        }

        if (14 == pid_b)
        {
            B_is_OK = GeantPIDBparent == 18;
        }
        else if (9 == pid_b)
        {
            B_is_OK = (GeantPIDB == 9 and GeantPIDBparent == 18) or
                        (GeantPIDB == 6 and GeantPIDBparent == 9 and GeantPIDBGparent == 18);
            ads.fGeaPiMuonLa = (GeantPIDB == 6 and GeantPIDBparent == 9 and GeantPIDBGparent == 18);
        }

        ads.fRealLambda = A_is_OK and B_is_OK;
        ads.fpartB_tof_pid = GeantPIDB;
    }
    else
    {
        ads.fRealLambda = kFALSE;
    }

    ads.fChiA = trackA.getChi2();
    ads.fChiB = trackB.getChi2();

    ads.fMetaMatchQA = trackA.getMetaMatchQuality();
//     ads.fMetaMatchQB = trackB.getMetaMatchQuality();

    dirMother.setXYZ(ads.trec_lambda.X(), ads.trec_lambda.Y(), ads.trec_lambda.Z());    // direction vector of the mother particle

    if (analysisType == KT::Sim)
    {
        if (14 == pid_a) {
            ads.fGeaVxLa = tcs_a->getGeantxVertex();
            ads.fGeaVyLa = tcs_a->getGeantyVertex();
            ads.fGeaVzLa = tcs_a->getGeantzVertex();
        } else if (14 == pid_b) {
            ads.fGeaVxLa = tcs_b->getGeantxVertex();
            ads.fGeaVyLa = tcs_b->getGeantyVertex();
            ads.fGeaVzLa = tcs_b->getGeantzVertex();
        }

        ads.fGeaVxLa = tcs_a->getGeantxVertex();
        ads.fGeaVyLa = tcs_a->getGeantyVertex();
        ads.fGeaVzLa = tcs_a->getGeantzVertex();

        TVector3 dv(ads.vx_lambda.X() - ads.fGeaVxLa,
                    ads.vx_lambda.Y() - ads.fGeaVyLa,
                    ads.vx_lambda.Z() - ads.fGeaVzLa);

        ads.fVMCD_La = dv.Mag();
        ads.fVMCD_La_x = dv.X();
        ads.fVMCD_La_y = dv.Y();
        ads.fVMCD_La_z = dv.Z();

        Float_t poca_t, poca_l, poca, poca_x, poca_y;
        TVector3 _b, _d;
        HGeomVector _bg, _dg;

        if (pid_a == 14)
            HParticleTool::calcSegVector(trackA.getZ(), trackA.getR(),
                                TMath::DegToRad()*trackA.getPhi(),
                                TMath::DegToRad()*trackA.getTheta(),
                                _bg, _dg);
        else
            HParticleTool::calcSegVector(trackB.getZ(), trackB.getR(),
                                TMath::DegToRad()*trackB.getPhi(),
                                TMath::DegToRad()*trackB.getTheta(),
                                _bg, _dg);

        _b.SetXYZ(_bg.X(), _bg.Y(), _bg.Z());
        _d.SetXYZ(_dg.X(), _dg.Y(), _dg.Z());

        TVector3 mc(ads.fGeaVxLa, ads.fGeaVyLa, ads.fGeaVzLa);
        POCA(mc, _b, _d, poca_t, poca_l, poca, poca_x, poca_y);

        ads.fLaPres = poca;
        ads.fLaPres_t = poca_t;
        ads.fLaPres_l = poca_l;
        ads.fLaPres_x = poca_x;
        ads.fLaPres_y = poca_y;
    }

    ads.fVertDistA = ads.trec_lambda.getMTDa();
    ads.fVertDistB = ads.trec_lambda.getMTDb();

//     ads.vx_primary = PrimVertexMother; FIXME
//     if ((ads.vx_lambda.Z() - ads.vx_primary.Z()) < 0)
//     {
// #ifdef SHOWREJECTED
//         ads.fRejected = ERR_VERTEX_Z_MISSMATCH;
// #else
// //         ads.ret = ERR_VERTEX_Z_MISSMATCH;            // FIXME and below
// //         return ads;
// #endif /*SHOWREJECTED*/
//     }

    ads.fVertDistA = ads.trec_lambda.getMTDa();
    ads.fVertDistB = ads.trec_lambda.getMTDb();

//     double dist2 = pow(PrimVertexMother.getX() - beamVector.getX(), 2.0) +
//         pow(PrimVertexMother.getY() - beamVector.getY(), 2.0);

//     if ( !(dist2 < 100.0 and PrimVertexMother.getZ() < 0.0 and PrimVertexMother.getZ() > -90.0) )
//         return ERR_NOT_IN_VERTEX;
//     if ( !(ads.vx_primary.getR() < 10.0 and ads.vx_primary.getZ() < 0.0 and ads.vx_primary.getZ() > -90.0) )
//     {
// #ifdef SHOWREJECTED
//         ads.fRejected = ERR_NOT_IN_VERTEX;
// #else
//         ads.ret = ERR_NOT_IN_VERTEX;
//         return ads;
// #endif /*SHOWREJECTED*/
//     }

    ads.fAngleAB    = trackA.Angle(trackB.Vect());
    ads.fRelAngleA  = ads.trec_lambda.Angle(trackA.Vect());
    ads.fRelAngleB  = ads.trec_lambda.Angle(trackB.Vect());

    //Boost in CMS: ***********************************************
    TLorentzVector trackAB_CMS = ads.trec_lambda;

    trackAB_CMS.Boost(-Vec_pp45_sum.BoostVector());
    ads.tr_lambda_cms = trackAB_CMS;

    TLorentzVector trackA_CMS = trackA;
    trackA_CMS.Boost(-Vec_pp45_sum.BoostVector());
    ads.fMomA_cms            = trackA_CMS.P();

    TLorentzVector trackB_CMS = trackB;
    trackB_CMS.Boost(-Vec_pp45_sum.BoostVector());
    ads.fMomB_cms            = trackB_CMS.P();

    //*************************************************************

    ads.fMt                = ads.trec_lambda.Mt();               // Transverse mass

    ads.fLambda_MM = (Vec_pp45_sum - ads.trec_lambda).M2();      // Missing Mass

    if (flag_nosecvertcuts == 0)
    {
    if (analysisType == KT::Sim)     //for simulated data
    {
    }
    else       //for experimental data
    {
    }
    }

    ++ads.fEventLCounter;

//     A_PID = KT::pip;
//     trackA = *(HParticleCandSim*)pcand->getObject(trackA_num);
//     trackA.calc4vectorProperties(HPhysicsConstants::mass(A_PID));
//     KTifini::CalcSegVector(trackA.getZ(), trackA.getR(), trackA.getPhi(), trackA.getTheta(), baseA, dirA);
//     ads.fMomA = trackA.getMomentum();
//
//     if (flag_elosscorr)
//     {
//         // with corr
//         momentum_A_corr = eLossCorr->getCorrMom(A_PID, ads.fMomA, trackA.getTheta());
//     }
//     else
//     {
//         // no corr
//         momentum_A_corr = ads.fMomA;
//     }
//
//     trackA.setMomentum(momentum_A_corr);
//
//     trackA.calc4vectorProperties(HPhysicsConstants::mass(A_PID));
//
//     TLorentzVector trackAB_miss = trackA + trackB;
//
//     ads.fM_miss = trackAB_miss.M();
//     ads.fPVA_miss = calcAngleVar(v1, trackAB_miss);
//
    ads.ret = ads.trec_lambda.M();
    return ads;
}

AnaDataSet ef_xim_pp45::singleFwDetPairAnalysisXi(HEvent* /*fEvent*/, Int_t /*event_num*/, const AnaDataSet& ads_a, UInt_t pid_b, int trackB_num, bool /*quick_run*/)
{
    AnaDataSet ads = ads_a;

    HGeomVector dirMother, PrimVertexMother;

    TObject * o_b = forward_tracks[trackB_num].cand;

    KTrackReconstructor trackA = ads_a.trec_lambda;
    HForwardCand trackB = *(HForwardCand*)o_b;

    if (trackB.getTofRec() == 0)
    {
    #ifdef SHOWREJECTED
        ads.fRejected = ERR_NO_FWDET_TOF;
    #else
        ads.ret = ERR_NO_FWDET_TOF;
        return ads;
    #endif /*SHOWREJECTED*/
    }

    trackB.calc4vectorProperties(HPhysicsConstants::mass(pid_b));

    ads.trec_xim.reconstruct(trackA, trackB);
    ads.vx_xim = ads.trec_xim.getDecayVertex();

    if (flag_refit_fwdet && trackB.getTofRec())
    {
        HCategory * pFRpcHit = gHades->getCurrentEvent()->getCategory(catFRpcHit);
        HFRpcHit * frpchit = dynamic_cast<HFRpcHit*>(pFRpcHit->getObject(trackB.getFRpcHitIndex()));
        HForwardTools::correctPathLength(&trackB, HGeomVector(ads.vx_xim.X(), ads.vx_xim.Y(), ads.vx_xim.Z()), frpchit);
        HForwardTools::correctTrackProperties(&trackB, HPhysicsConstants::mass(14));

        ads.trec_xim.reconstruct(trackA, trackB);
        ads.vx_xim = ads.trec_xim.getDecayVertex();
    }
    ads.tr_xim_c = trackB;

    if (trackB.getTofRec() <= 0)
    {
    #ifdef SHOWREJECTED
        ads.fRejected = ERR_NO_FWDET_TOF;
    #else
        ads.ret = ERR_NO_FWDET_TOF;
        return ads;
    #endif /*SHOWREJECTED*/
    }

    ads.fXim_MTD = ads.trec_xim.getMTD();

    // distance between Xi- and Lambda
    HGeomVector v1 = ads.vx_lambda - ads.vx_xim;
    ads.fVertDistX = (v1).length();
    ads.fLambda_PVA = calcAngleVar(v1, ads.trec_lambda);

    // cms system
    TLorentzVector trackLC_CMS = ads.trec_xim;
    trackLC_CMS.Boost(-Vec_pp45_sum.BoostVector());
    ads.tr_xim_cms = trackLC_CMS;

    TLorentzVector trackC_CMS = trackB;
    trackC_CMS.Boost(-Vec_pp45_sum.BoostVector());
    ads.fMomC_cms            = trackC_CMS.P();

    // I do not need so many data!
    KCutInside<Float_t> xi_mass_test(XI_MASS - 100.0, XI_MASS + 100.0, KT::WEAK, KT::WEAK);

    if (!xi_mass_test.test(ads.trec_xim.M()))
    {
    #ifdef SHOWREJECTED
        ads.fRejected = ERR_MASS_OUT_OF_RANGE;
    #else
        ads.ret = ERR_MASS_OUT_OF_RANGE;
        return ads;
    #endif /*SHOWREJECTED*/
    }

    // extra checks for the simulation analysis
    HVirtualCandSim * tcs_b = dynamic_cast<HVirtualCandSim*>(o_b);

    if (analysisType == KT::Sim && tcs_b)
    {
        int GeantPIDB = tcs_b->getGeantPID();
        int GeantPIDBparent = tcs_b->getGeantParentPID();
        int GeantPIDBGparent = tcs_b->getGeantGrandParentPID();

        ads.fRealXim = (
            // proton-pim pair
            (ads.fRealLambda and GeantPIDB == 9 and GeantPIDBparent == 23)
            or
            // proton-pi- (decaying into mu-)
            (ads.fRealLambda and GeantPIDB == 6 and GeantPIDBparent == 9 and GeantPIDBGparent == 23)
        );
        ads.fGeaPiMuonXim = (GeantPIDB == 6 and GeantPIDBparent == 9 and GeantPIDBGparent == 23);

        ads.fGeaVxXim = tcs_b->getGeantxVertex();
        ads.fGeaVyXim = tcs_b->getGeantyVertex();
        ads.fGeaVzXim = tcs_b->getGeantzVertex();

        TVector3 dv(ads.vx_xim.X() - ads.fGeaVxXim,
                    ads.vx_xim.Y() - ads.fGeaVyXim,
                    ads.vx_xim.Z() - ads.fGeaVzXim);

        ads.fVMCD_Xim = dv.Mag();
        ads.fVMCD_Xim_x = dv.X();
        ads.fVMCD_Xim_y = dv.Y();
        ads.fVMCD_Xim_z = dv.Z();
    }

    ads.fXim_MM = (Vec_pp45_sum - ads.trec_xim).M2();        // Missing Mass

    ads.ret = ads.trec_xim.M();
    return ads;
}

void ef_xim_pp45::configureTree(TTree * tree)
{
    trec_lambda.setTree(tree, "Lambda_");
    track_lambda_cms.setTree(tree, "Lambda_cms_", KTrack::bCosTheta | KTrack::bP | KTrack::bY);
    vertex_lambda.setTree(tree, "LambdaDecay", KVertex::bXYZ | KVertex::bR);
    track_lambda_a.setTree(tree, "partA_", 0x1ff);
    track_lambda_b.setTree(tree, "partB_", 0x1ff);

    trec_xim.setTree(tree, "Xim_");
    track_xim_cms.setTree(tree, "Xim_cms_", KTrack::bCosTheta | KTrack::bP | KTrack::bY);
    vertex_xim.setTree(tree, "XimDecay", KVertex::bXYZ | KVertex::bR);
    track_xim_c.setTree(tree, "partC_", 0x1ff);

    vertex_primary.setTree(tree, "PrimaryVertex", KVertex::bXYZ | KVertex::bR);

    tree->Branch("fLambda_MM",      &g_ads.fLambda_MM,      "fLambda_MM/F");
    tree->Branch("fXim_MM",         &g_ads.fXim_MM,         "fXim_MM/F");

    tree->Branch("fMt",             &g_ads.fMt,             "fMt/F");

    tree->Branch("fLambda_MTD",     &g_ads.fLambda_MTD,     "fLambda_MTD/F" );
    tree->Branch("fXim_MTD",        &g_ads.fXim_MTD,        "fXi_MTD/F" );
    tree->Branch("fVertDistX",      &g_ads.fVertDistX,      "fVertDistX/F");
    tree->Branch("fLambda_PVA",     &g_ads.fLambda_PVA,     "fLambda_PVA/F");
    tree->Branch("fXim_PVA",        &g_ads.fXim_PVA,        "fXim_PVA/F");
    tree->Branch("fPVA_miss",       &g_ads.fPVA_miss,       "fPVA_miss/F");

    tree->Branch("fFitVertexX",     &g_ads.fFitVertexX,     "fFitVertexX/F");
    tree->Branch("fFitVertexY",     &g_ads.fFitVertexY,     "fFitVertexY/F");
    tree->Branch("fFitVertexZ",     &g_ads.fFitVertexZ,     "fFitVertexZ/F");

    tree->Branch("fEventVertexX",   &g_ads.fEventVertexX,   "fEventVertexX/F");
    tree->Branch("fEventVertexY",   &g_ads.fEventVertexY,   "fEventVertexY/F");
    tree->Branch("fEventVertexZ",   &g_ads.fEventVertexZ,   "fEventVertexZ/F");

    tree->Branch("fVMCD_La",        &g_ads.fVMCD_La,        "fVMCD_La/F");
    tree->Branch("fVMCD_La_x",      &g_ads.fVMCD_La_x,      "fVMCD_La_x/F");
    tree->Branch("fVMCD_La_y",      &g_ads.fVMCD_La_y,      "fVMCD_La_y/F");
    tree->Branch("fVMCD_La_z",      &g_ads.fVMCD_La_z,      "fVMCD_La_z/F");
    tree->Branch("fVMCD_Xim",       &g_ads.fVMCD_Xim,       "fVMCD_Xim/F");
    tree->Branch("fVMCD_Xim_x",     &g_ads.fVMCD_Xim_x,     "fVMCD_Xim_x/F");
    tree->Branch("fVMCD_Xim_y",     &g_ads.fVMCD_Xim_y,     "fVMCD_Xim_y/F");
    tree->Branch("fVMCD_Xim_z",     &g_ads.fVMCD_Xim_z,     "fVMCD_Xim_z/F");
    tree->Branch("fLaPres",         &g_ads.fLaPres,         "fLaPres/F");
    tree->Branch("fLaPres_t",       &g_ads.fLaPres_t,       "fLaPres_t/F");
    tree->Branch("fLaPres_l",       &g_ads.fLaPres_l,       "fLaPres_l/F");
    tree->Branch("fLaPres_x",       &g_ads.fLaPres_x,       "fLaPres_x/F");
    tree->Branch("fLaPres_y",       &g_ads.fLaPres_y,       "fLaPres_y/F");

    tree->Branch("fMomA_cms",       &g_ads.fMomA_cms,       "fMomA_cms/F");
    tree->Branch("fMomB_cms",       &g_ads.fMomB_cms,       "fMomB_cms/F");
    tree->Branch("fMomC_cms",       &g_ads.fMomC_cms,       "fMomC_cms/F");
    tree->Branch("fAngleAB",        &g_ads.fAngleAB,        "fAngleAB/F");
    tree->Branch("fRelAngleA",      &g_ads.fRelAngleA,      "fRelAngleA/F");
    tree->Branch("fRelAngleB",      &g_ads.fRelAngleB,      "fRelAngleB/F");

    tree->Branch("fChiA",           &g_ads.fChiA,           "fChiA/F");
    tree->Branch("fChiB",           &g_ads.fChiB,           "fChiB/F");
    tree->Branch("fChiC",           &g_ads.fChiC,           "fChiC/F");
    tree->Branch("fMetaMatchQA",    &g_ads.fMetaMatchQA,    "fMetaMatchQA/F");
    tree->Branch("fMetaMatchQB",    &g_ads.fMetaMatchQB,    "fMetaMatchQB/F");
    tree->Branch("fVertDistA",      &g_ads.fVertDistA,      "fVertDistA/F");
    tree->Branch("fVertDistB",      &g_ads.fVertDistB,      "fVertDistB/F");
    tree->Branch("fVertDistC",      &g_ads.fVertDistC,      "fVertDistC/F");
    tree->Branch("fpartB_tof",      &g_ads.fpartB_tof,      "fpartB_tof/F");
    tree->Branch("fpartB_tof_pid",  &g_ads.fpartB_tof_pid,  "fpartB_tof_pid/I");
    tree->Branch("fHadesTracksNum", &g_ads.fHadesTracksNum, "fHadesTracksNum/I");
    tree->Branch("fFwDetTracksNum", &g_ads.fFwDetTracksNum, "fFwDetTracksNum/I");
    tree->Branch("fIsFwDetData",    &g_ads.fIsFwDetData,    "fIsFwDetData/I");
    tree->Branch("fIsFwDetDataXi",  &g_ads.fIsFwDetDataXi,  "fIsFwDetDataXi/I");

    tree->Branch("fMultA",          &g_ads.fMultA,          "fMultA/I");
    tree->Branch("fMultB",          &g_ads.fMultB,          "fMultB/I");
    tree->Branch("fMultC",          &g_ads.fMultC,          "fMultC/I");

    tree->Branch("fEventLCounter",  &g_ads.fEventLCounter,  "fEventLCounter/I");

    if (analysisType == KT::Sim)
    {
        tree->Branch("fRealLambda",     &g_ads.fRealLambda,     "fRealLambda/I");
        tree->Branch("fRealXim",        &g_ads.fRealXim,        "fXimLambda/I");
        tree->Branch("fPrimLambda",     &g_ads.fPrimLambda,     "fPrimLambda/I");
        tree->Branch("fGeantInfoNum",   &g_ads.fGeantInfoNum,   "fGeantInfoNum/I");
        tree->Branch("fGeantWeight",    &g_ads.fGeantWeight,    "fGeantWeight/F");

        tree->Branch("fGeaP",           &g_ads.fGeaP,           "fGeaP/F");
        tree->Branch("fGeaPx",          &g_ads.fGeaPx,          "fGeaPx/F");
        tree->Branch("fGeaPy",          &g_ads.fGeaPy,          "fGeaPy/F");
        tree->Branch("fGeaPz",          &g_ads.fGeaPz,          "fGeaPz/F");
        tree->Branch("fGeaVxLa",        &g_ads.fGeaVxLa,        "fGeaVxLa/F");
        tree->Branch("fGeaVyLa",        &g_ads.fGeaVyLa,        "fGeaVyLa/F");
        tree->Branch("fGeaVzLa",        &g_ads.fGeaVzLa,        "fGeaVzLa/F");
        tree->Branch("fGeaVxXim",       &g_ads.fGeaVxXim,       "fGeaVxXim/F");
        tree->Branch("fGeaVyXim",       &g_ads.fGeaVyXim,       "fGeaVyXim/F");
        tree->Branch("fGeaVzXim",       &g_ads.fGeaVzXim,       "fGeaVzXim/F");
        tree->Branch("fGeaTheta",       &g_ads.fGeaTheta,       "fGeaTheta/F");
        tree->Branch("fGeaPhi",         &g_ads.fGeaPhi,         "fGeaPhi/F");
        tree->Branch("fGeaXf",          &g_ads.fGeaXf,          "fGeaXf/F");
        tree->Branch("fGeaAngleAB",     &g_ads.fGeaAngleAB,     "fGeaAngleAB/F");
        tree->Branch("fGeaPiMuonLa",    &g_ads.fGeaPiMuonLa,    "fGeaPiMuonLa/I");
        tree->Branch("fGeaPiMuonXim",   &g_ads.fGeaPiMuonXim,   "fGeaPiMuonXim/I");
    }

    tree->Branch("fPVtype",         &g_ads.fPVtype,         "fPVtype/I");

#ifdef SHOWREJECTED
    tree->Branch("fRejected",       &g_ads.fRejected,       "fRejected/I");
#endif /*SHOWREJECTED*/

    tree->Branch("fSortOrderMtd",   &g_ads.fSortOrderMtd,   "fSortOrderMtd/I");
    tree->Branch("fEventSeqNumber", &g_ads.fEventSeqNumber, "fEventSeqNumber/i");
}

void ef_xim_pp45::configureGraphicalCuts(KTrackInspector & trackInsp)
{
    const TString jsieben_pNb_cuts = "/scratch/e12f/knucl/jsieben/pNb/Cuts/";
    const TString aschmah_pp35_cuts = "/scratch/e12f/schmah/GraphicalCuts/pp35/";
    const TString jchen_pp35_cuts = "/home/gu27buz/hadesdst/pp35/";
    const TString jchen_pp35_cuts_sim = "/scratch/e12l/knucl/hades/jchen/pp35/GraphicalCuts/Sim/";
    const TString jchen_pp35_cuts_sim2 = "/scratch/e12f/knucl/rlalik/pp35/LambdaAnalysis/graph_cuts/";//"/scratch/e12l/knucl/hades/jchen/pp35/new_backup/GraphicalCuts/Sim/";

//    setChargePID(kTRUE);

    const TString rlalik_cuts = "/scratch/e12m/knucl/rlalik/pp35/LambdaAnalysis/Exp/";
    if (analysisType == KT::Sim)
    {
    // protons
//         trackInsp.registerCut(KT::MDC, KT::cut_p, jchen_pp35_cuts_sim2 + "Modified_PID_Cuts_Poly5_ChiiV5_Meth2.root", "Mdc_dEdx_P_cut_mod_ChiiV1_Sim_mod");
    // pions-
//         trackInsp.registerCut(KT::MDC, KT::cut_pim, jchen_pp35_cuts_sim2 + "Modified_PID_Cuts_Poly5_ChiiV5_Meth2.root", "Mdc_dEdx_PiP_cut_PID_mod_ChiiV2_Sim_mod_PiM");

//         trackInsp.registerdEdxPlot(KT::TOF);
//         trackInsp.registerdEdxPlot(KT::TOFINO);
    }
    else if (analysisType == KT::Exp)
    {
    // protons
//         trackInsp.registerCut(KT::MDC, KT::cut_p, jchen_pp35_cuts + "Mdc_dEdx_P_cut_mod_ChiiV1.root", "Mdc_dEdx_P_cut_mod_ChiiV1");
    // pions-
//         trackInsp.registerCut(KT::MDC, KT::cut_pim, jchen_pp35_cuts + "Mdc_dEdx_PiP_cut_PID_mod_ChiiV2.root", "Mdc_dEdx_PiP_cut_PID_mod_ChiiV2", kFALSE);    // new cat is already for pim
    }

    trackInsp.configureMetaSystem(KT::cut_p, KT::MDC);
    trackInsp.configureMetaSystem(KT::cut_pim, KT::MDC);
}

void ef_xim_pp45::initAnalysis(KT::Experiment exp, KT::AnalysisType analysisType)
{
    KAbstractAnalysis::initAnalysis(exp, analysisType);

    refBeamVector = getTargetGeomVector();
    beamCal = new KBeamCalibration(getTargetGeometry());
//     beamCal->initBeamCorrArray(analysisType);

    std::cout << "++ Analysis configuration" << std::endl;
    std::cout << "    nosecvertcuts    : " << flag_nosecvertcuts << std::endl;
    std::cout << "    elosscorr        : " << flag_elosscorr << std::endl;
    std::cout << "    no-sigmas        : " << flag_nosigmas << std::endl;
    std::cout << "    use-event-vertex : " << flag_useeventvertex << std::endl;
    std::cout << "    use-wall         : " << flag_usewall << std::endl;
    std::cout << "    Mtd              : " << par_Mtd << std::endl;
    std::cout << "    VertexDistX      : " << par_VertDistX << std::endl;
    std::cout << "    VertexDistA      : " << par_VertDistA << std::endl;
    std::cout << "    VertexDistB      : " << par_VertDistB << std::endl;
}

void ef_xim_pp45::finalizeAnalysis()
{
    delete beamCal;
    KAbstractAnalysis::finalizeAnalysis();
}

void ef_xim_pp45::ConfigureOptions(int& n, option* & long_options)
{
    static struct option _long_options[] =
    {
        /* These options set a flag. */
        { "nosecvercuts",       no_argument,        &flag_nosecvertcuts,    1 },
        { "elosscorr",          no_argument,        &flag_elosscorr,        1 },
        { "no-sigmas",          no_argument,        &flag_nosigmas,         1 },
        { "use-event-vertex",   no_argument,        &flag_useeventvertex,   1 },
        { "use-wall",           no_argument,        &flag_usewall,          1 },
        { "refit-fwdet",        no_argument,        &flag_refit_fwdet,      1 },
        /* These options don't set a flag.
        We distingu*ish them by their indices. */
//            {"events",     no_argument,       0, 'e'},
        { "Mtd",                required_argument,  0, 'm'},
        { "VertDistX",          required_argument,  0, 'x'},
        { "VertDistA",          required_argument,  0, 'p'},
        { "VertDistB",          required_argument,  0, 'q'},
    };

    long_options = _long_options;
    n = sizeof(_long_options)/sizeof(option);
}


int ef_xim_pp45::Configure(int c, const char * optarg)
{
    switch (c) {
        case 'm':
            par_Mtd = atol(optarg);
            break;
        case 'x':
            par_VertDistX = atol(optarg);
            break;
        case 'p':
            par_VertDistA = atol(optarg);
            break;
        case 'q':
            par_VertDistB = atol(optarg);
            break;

        default:
            abort();
            break;
    }

    return c;
}

void ef_xim_pp45::Usage() const
{
    std::cout <<
    "Analysis options: \n" <<
    "      --nosecvercuts\t\t\t - disable secondary vertex cuts\n" <<
    "\n\n";
}

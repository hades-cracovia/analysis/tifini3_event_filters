/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Rafał Lalik <rafal.lalik@@ph.tum.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef PP45_XIM_H
#define PP45_XIM_H

#include "henergylosscorrpar.h"

#include "KAbstractAnalysis.h"
#include "KTifiniAnalysis.h"
#include "KBeamCalibration.h"
#include "KVertex.h"
#include "KVector.h"
#include "KTrack.h"
#include "KTrackReconstructor.h"
#include "KCutInside.h"

// constants
static const Float_t NULL_DATA = -10000.;
static const Float_t MASS_OK = 10000.;
static const Float_t ERR_NOT_IN_VERTEX = -10001.;
static const Float_t ERR_MASS_OUT_OF_RANGE = -10002.;
static const Float_t ERR_VERTEX_Z_MISSMATCH = -10004.;
static const Float_t ERR_SIM_VERTEX_MISSMATCH = -10005.;
static const Float_t ERR_NO_FWDET_TOF = -10006.;

//#define SHOWREJECTED

struct AnaDataSet
{
    // Kinematic properties
    Float_t fLambda_MM;
    Float_t fXim_MM;
    Float_t fMt;

    // track-reco properties
    Float_t fLambda_MTD;
    Float_t fXim_MTD;
    Float_t fVertDistX;
    Float_t fLambda_PVA;
    Float_t fXim_PVA;
    Float_t fPVA_miss;

    // vertex properties
    Float_t fFitVertexX;
    Float_t fFitVertexY;
    Float_t fFitVertexZ;

    Float_t fEventVertexX;
    Float_t fEventVertexY;
    Float_t fEventVertexZ;

    Float_t fVMCD_La;
    Float_t fVMCD_La_x;
    Float_t fVMCD_La_y;
    Float_t fVMCD_La_z;
    Float_t fVMCD_Xim;
    Float_t fVMCD_Xim_x;
    Float_t fVMCD_Xim_y;
    Float_t fVMCD_Xim_z;

    Float_t fLaPres;
    Float_t fLaPres_t;
    Float_t fLaPres_l;
    Float_t fLaPres_x;
    Float_t fLaPres_y;

    // daughter properties
//     Float_t fMomAx, fMomAy, fMomAz, fMomBx, fMomBy, fMomBz, fMomCx, fMomCy, fMomCz;
    Float_t fMomA_cms, fMomB_cms, fMomC_cms;
    Float_t fAngleAB;
    Float_t fRelAngleA, fRelAngleB;
    Float_t fChiA, fChiB, fChiC;
    Float_t fMetaMatchQA, fMetaMatchQB;
    Float_t fVertDistA, fVertDistB, fVertDistC;
    Int_t fMultA, fMultB, fMultC;
    Float_t fpartB_tof;
    Int_t fpartB_tof_pid;

    // event stats
    Int_t fHadesTracksNum;
    Int_t fFwDetTracksNum;
    Int_t fIsFwDetData;
    Int_t fIsFwDetDataXi;
    Int_t fEventLCounter;
    Int_t fRealLambda;
    Int_t fRealXim;
    Int_t fPrimLambda;

    // other
    Int_t fGeantInfoNum;
    Float_t fGeantWeight;

    Float_t ret;
    Int_t fSortOrderMtd;
#ifdef SHOWREJECTED
    Int_t fRejected;
#endif

    // Geant
    Float_t fGeaP, fGeaPx, fGeaPy, fGeaPz;
    Float_t fGeaVxLa, fGeaVyLa, fGeaVzLa;
    Float_t fGeaVxXim, fGeaVyXim, fGeaVzXim;
    Float_t fGeaTheta, fGeaPhi;
    Float_t fGeaXf;
    Float_t fGeaAngleAB;
    Int_t fGeaPiMuonLa;
    Int_t fGeaPiMuonXim;

    Int_t fPVtype;
    UInt_t fEventSeqNumber;

    KTrack tr_lambda_cms;
    KTrack tr_lambda_a;
    KTrack tr_lambda_b;
    KTrack tr_xim_cms;
    KTrack tr_xim_c;

    KVertex vx_lambda;
    KVertex vx_xim;
    KVertex vx_primary;

    KTrackReconstructor trec_lambda;
    KTrackReconstructor trec_xim;

    void clear()
    {
        fHadesTracksNum = 0;
        fFwDetTracksNum = 0;
        fIsFwDetData = 0;

        fGeantWeight = 1.0;
        fEventVertexX = 0.0;
        fEventVertexY = 0.0;
        fEventVertexZ = 0.0;

        fEventLCounter = 0;

        fMultA = 0;
        fMultB = 0;
        fMultC = 0;
    }

    void init()
    {
        fLambda_MM = NULL_DATA;
        fXim_MM = NULL_DATA;
        fMt = NULL_DATA;

        fLambda_MTD = NULL_DATA;
        fXim_MTD = NULL_DATA;
        fVertDistX = NULL_DATA;
        fLambda_PVA = NULL_DATA;
        fXim_PVA = NULL_DATA;
        fPVA_miss = NULL_DATA;

        fFitVertexX = NULL_DATA;
        fFitVertexY = NULL_DATA;
        fFitVertexZ = NULL_DATA;

        fVMCD_La = NULL_DATA;
        fVMCD_La_x = NULL_DATA;
        fVMCD_La_y = NULL_DATA;
        fVMCD_La_z = NULL_DATA;
        fVMCD_Xim = NULL_DATA;
        fVMCD_Xim_x = NULL_DATA;
        fVMCD_Xim_y = NULL_DATA;
        fVMCD_Xim_z = NULL_DATA;

        fLaPres = NULL_DATA;
        fLaPres_t = NULL_DATA;
        fLaPres_l = NULL_DATA;
        fLaPres_x = NULL_DATA;
        fLaPres_y = NULL_DATA;

//         fMomAx = fMomAy = fMomAz = fMomBx = fMomBy = fMomBz = fMomCx = fMomCy = fMomCz = NULL_DATA;
        fMomA_cms = fMomB_cms = fMomC_cms = NULL_DATA;

        fAngleAB = NULL_DATA;
        fRelAngleA = fRelAngleB = NULL_DATA;
        fChiA = fChiB = fChiC = NULL_DATA;
        fMetaMatchQA = fMetaMatchQB = NULL_DATA;
        fVertDistA = fVertDistB = fVertDistC = NULL_DATA;
        fpartB_tof = NULL_DATA;
        fpartB_tof_pid = NULL_DATA;

        fGeantInfoNum = 0;
        fRealLambda = NULL_DATA;
        fRealXim = NULL_DATA;
        fPrimLambda = NULL_DATA;

        fGeaP = fGeaPx = fGeaPy = fGeaPz = NULL_DATA;
        fGeaVxLa = fGeaVyLa = fGeaVzLa = NULL_DATA;
        fGeaVxXim = fGeaVyXim = fGeaVzXim = NULL_DATA;
        fGeaTheta = fGeaPhi = NULL_DATA;
        fGeaXf = NULL_DATA;
        fGeaAngleAB = NULL_DATA;
        fGeaPiMuonLa = NULL_DATA;
        fGeaPiMuonXim = NULL_DATA;

        fPVtype = NULL_DATA;

#ifdef SHOWREJECTED
        fRejected = 0;
#endif /*SHOWREJECTED*/

        ret = 0.;
        fSortOrderMtd = 0;
    }
};

class ef_xim_pp45 : public KAbstractAnalysis
{
public:
    ef_xim_pp45(const TString & analysisName, const TString & treeName);
    virtual bool analysis(HEvent * fEvent, Int_t event_num, Int_t run_id);

    void initAnalysis(KT::Experiment exp, KT::AnalysisType analysisType);
    void finalizeAnalysis();

    void ConfigureOptions(int & n, option * & long_options);
    int Configure(int c, const char * optarg);
    void Usage() const;

protected:
    void configureTree(TTree * tree);
    void configureGraphicalCuts(KTrackInspector & cuts);

    AnaDataSet singleHadesPairAnalysis(HEvent * fEvent, Int_t event_num, UInt_t pid_a, UInt_t pid_b, int trackA_num, int trackB_num, bool quick_run = false);
    AnaDataSet singleFwDetPairAnalysis(HEvent * fEvent, Int_t event_num, UInt_t pid_a, UInt_t pid_b, int trackA_num, int trackB_num, bool quick_run = false);
    AnaDataSet singleHadesPairAnalysisXi(HEvent * fEvent, Int_t event_num, const AnaDataSet & ads_a, UInt_t pid_b, int trackB_num, bool quick_run = false);
    AnaDataSet singleFwDetPairAnalysisXi(HEvent * fEvent, Int_t event_num, const AnaDataSet & ads_a, UInt_t pid_b, int trackB_num, bool quick_run = false);

    HEnergyLossCorrPar * eLossCorr;
    KBeamCalibration * beamCal;
    HGeomVector refBeamVector;
    HGeomVector beamVector;
    TLorentzVector Vec_pp45_sum;

    // opts
    int flag_nosecvertcuts;
    int flag_elosscorr;
    int flag_nosigmas;
    int flag_useeventvertex;
    int flag_usewall;
    int flag_refit_fwdet;

    int par_Mtd;			// CLI
    int par_VertDistX;		// CLI
    int par_VertDistA;		// CLI
    int par_VertDistB;		// CLI

    KTrack track_lambda_cms;
    KTrack track_lambda_a;
    KTrack track_lambda_b;
    KTrack track_xim_cms;
    KTrack track_xim_c;
    KVertex vertex_lambda;
    KVertex vertex_xim;
    KVertex vertex_primary;

    KTrackReconstructor trec_lambda;
    KTrackReconstructor trec_xim;
};

#endif // PP45_XIM_H

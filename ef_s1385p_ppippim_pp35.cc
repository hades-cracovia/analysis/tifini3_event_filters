/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Rafał Lalik <rafal.lalik@ph.tum.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "ef_s1385p_ppippim_pp35.h"

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <getopt.h>

#include "hphysicsconstants.h"

#include "KTools.h"
#include "KTrackInspector.h"
#include "KBeamCalibrationApr07.h"
#include "KCutInside.h"

#ifdef SHOWREJECTED
//int fRejected;
#endif /*SHOWREJECTED*/

static const float E_kin_beam       = 3500.0;
static const float E_kin_target     = 0.0;
static const float E_total_beam     = E_kin_beam + HPhysicsConstants::mass(14);
static const float E_total_target   = E_kin_target + HPhysicsConstants::mass(14);
static const float pz_beam          = sqrt(E_total_beam*E_total_beam-HPhysicsConstants::mass(14)*HPhysicsConstants::mass(14));

static const float LAMBDA_MASS = 1115.9;
static const float S1385P_MASS = 1385.0;

static const float D2R = TMath::DegToRad();
static const float R2D = TMath::RadToDeg();

static AnaDataSet g_ads;

float calcAngleVar(HGeomVector & v1, HGeomVector & v2)
{
    TVector3 _v1; _v1.SetXYZ(v1.X(), v1.Y(), v1.Z());
    TVector3 _v2; _v2.SetXYZ(v2.X(), v2.Y(), v2.Z());

    return _v1.Angle(_v2);
}

float calcAngleVar(HGeomVector & v1, TLorentzVector & v2)
{
    TVector3 _v1; _v1.SetXYZ(v1.X(), v1.Y(), v1.Z());
    TVector3 _v2; _v2.SetXYZ(v2.Px(), v2.Py(), v2.Pz());

    return _v1.Angle(_v2);
}

ef_s1385p_ppippim_pp35::ef_s1385p_ppippim_pp35(const TString& analysisName, const TString& treeName) : KAbstractAnalysis(analysisName, treeName),
    flag_nosecvertcuts(0), flag_elosscorr(0), flag_nosigmas(0), flag_useeventvertex(0), par_Mtd(10), par_VertDistX(45), par_VertDistA(5), par_VertDistB(15)
{
//     setGoodEventSelector(Particle::kGoodTRIGGER |
//         Particle::kGoodVertexClust |
// //         Particle::kGoodVertexCand |
//         Particle::kGoodSTART |
//         Particle::kNoPileUpSTART);

    setExperimentType(KT::apr07);
    setGoodEventSelector(0);
    setDownscalingSelector(KT::OnlyDownscaled);

    setPidSelectionHades(KT::p, /*KT::Beta | KT::Charge |*/ KT::Graphical_dEdx);
    setPidSelectionHades(KT::pim, /*KT::Beta | KT::Charge |*/ KT::Graphical_dEdx);
    setPidSelectionHades(KT::pip, /*KT::Beta | KT::Charge |*/ KT::Graphical_dEdx);

    eLossCorr = new HEnergyLossCorrPar("eLossCorr", "eLossCorr", "eLossCorr");
    eLossCorr->setDefaultPar("jan04");

    cut_counters[NULL_DATA] = 0;
    cut_counters[MASS_OK] = 0;
    cut_counters[ERR_NOT_IN_VERTEX] = 0;
    cut_counters[ERR_MASS_OUT_OF_RANGE] = 0;
    cut_counters[ERR_NO_LAMBDA] = 0;
    cut_counters[ERR_VERTEX_Z_MISSMATCH] = 0;
    cut_counters[ERR_SIM_VERTEX_MISSMATCH] = 0;
    cut_counters[ERR_BAD_MMQ] = 0;
}

ef_s1385p_ppippim_pp35::~ef_s1385p_ppippim_pp35()
{
    printf("-------------------------------------------------\n");
    printf("Cuts statistics:\n");
    std::map<Float_t, Long_t>::const_iterator it = cut_counters.begin();
    for (; it != cut_counters.end(); ++it)
    {
        printf("%f\t: %ld\n", it->first, it->second);
    }
    printf("-------------------------------------------------\n");
}

bool ef_s1385p_ppippim_pp35::analysis(HEvent * event, Int_t event_num, Int_t run_id)
{
    if ( (hades_tracks.size()) < 3 )
        return false;

    int A_PID = KT::p;      // proton
    int B_PID = KT::pim;    // pi-
    int C_PID = KT::pip;    // pi+

    g_ads.fHadesTracksNum = hades_tracks.size();

    g_ads.fGeantWeight = 0;

    g_ads.fEventVertexX = event->getHeader()->getVertex().getX();
    g_ads.fEventVertexY = event->getHeader()->getVertex().getY();
    g_ads.fEventVertexZ = event->getHeader()->getVertex().getZ();

    g_ads.fEventLCounter = 0;

    g_ads.fMultA = 0;
    g_ads.fMultB = 0;

    std::vector<AnaDataSet> ads_arr;
    ads_arr.reserve(10000);

    size_t combo_cnt = 0;
    std::vector<float> mtd_list;

    beamVector = refBeamVector;
//     if (analysisType == KT::Exp)
//     {
//         beamVector = beamCal->calculateBeamOffset(event->getRunId());
//     } else {
        beamVector = refBeamVector;
//     }

    TVector3 p_beam_vec(0.0, 0.0, pz_beam);
    TLorentzVector Vec_pp35_beam    = TLorentzVector(p_beam_vec.X(), p_beam_vec.Y(), p_beam_vec.Z(), E_total_beam);
    TLorentzVector Vec_pp35_target  = TLorentzVector(0.0, 0.0, 0.0, E_total_target);
    Vec_pp35_sum       = Vec_pp35_beam + Vec_pp35_target;

    for(int i = 0; i < /*fMultA*/g_ads.fHadesTracksNum; ++i)
    {
        if (!hades_tracks[i].pid[A_PID][KT::Graphical_dEdx])
            continue;

        for(int j = 0; j < /*fMultB*/g_ads.fHadesTracksNum; ++j)
        {
            if (i == j)
                continue;

            if (!hades_tracks[j].pid[B_PID][KT::Graphical_dEdx])
                continue;

            AnaDataSet ads_ret = singlePairAnalysis(event, run_id, A_PID, B_PID, i, j);
            ++cut_counters[ads_ret.ret];

            if (ads_ret.ret == ERR_MASS_OUT_OF_RANGE)
                continue;

            if (ads_ret.ret == ERR_NO_LAMBDA)
                continue;

            if (ads_ret.ret <= NULL_DATA)
                continue;

            // H-H-H
            for(int k = 0; k < g_ads.fHadesTracksNum; ++k)
            {
                if (k == j or k == i)
                    continue;

                if (!hades_tracks[k].pid[C_PID][KT::Graphical_dEdx])
                    continue;

                ++cut_counters[-1.];
                AnaDataSet ads_s1385p_ret = singleHadesPairAnalysisS1385p(event, event_num, ads_ret, C_PID, k);
                if (ads_s1385p_ret.ret <= NULL_DATA)
                    ++cut_counters[ads_s1385p_ret.ret];


                if (ads_s1385p_ret.ret == ERR_MASS_OUT_OF_RANGE)
                    continue;

                if (ads_s1385p_ret.ret <= NULL_DATA)
                    continue;

                hades_tracks[i].is_used = true;
                hades_tracks[j].is_used = true;
                hades_tracks[k].is_used = true;

                ads_arr.push_back(ads_s1385p_ret);
                ++combo_cnt;
            }
        }
    }

    for (size_t i = 0; i < combo_cnt; ++i)
    {
        g_ads = ads_arr[i];

        track_lambda_cms = ads_arr[i].tr_lambda_cms;
        track_lambda_a = ads_arr[i].tr_lambda_a;
        track_lambda_a_cms = ads_arr[i].tr_lambda_a_cms;
        track_lambda_b = ads_arr[i].tr_lambda_b;
        track_lambda_b_cms = ads_arr[i].tr_lambda_b_cms;
        track_s1385p_cms = ads_arr[i].tr_s1385p_cms;
        track_s1385p_c = ads_arr[i].tr_s1385p_c;
        track_s1385p_c_cms = ads_arr[i].tr_s1385p_c_cms;
        vertex_lambda = ads_arr[i].vx_lambda;
        vertex_s1385p = ads_arr[i].vx_s1385p;
        trec_lambda = ads_arr[i].trec_lambda;
        trec_s1385p = ads_arr[i].trec_s1385p;

        track_lambda_cms.fill();
        track_lambda_a.fill();
        track_lambda_a_cms.fill();
        track_lambda_b.fill();
        track_lambda_b_cms.fill();

        track_s1385p_cms.fill();
        track_s1385p_c.fill();
        track_s1385p_c_cms.fill();

        vertex_lambda.fill();
        vertex_s1385p.fill();

        trec_lambda.fill();
        trec_s1385p.fill();

        getTree()->Fill();
    }

    return true;
}

AnaDataSet ef_s1385p_ppippim_pp35::singlePairAnalysis(HEvent * /*event*/, Int_t run_id, UInt_t pid_a, UInt_t pid_b, int trackA_num, int trackB_num, bool quick_run)
{
    HGeomVector beamVector;
    if (analysisType == KT::Exp)
    {
        beamVector = beamCal->calculateBeamOffset(run_id);
    } else {
        beamVector = refBeamVector;
    }
//printf("%f %f %f\n", beamVector.X(), beamVector.Y(), beamVector.Z());

    TVector3 p_beam_vec(0.0, 0.0, pz_beam);
    TLorentzVector Vec_pp35_beam    = TLorentzVector(p_beam_vec.X(), p_beam_vec.Y(), p_beam_vec.Z(), E_total_beam);

    TLorentzVector vec_beam_cms             = Vec_pp35_beam;
    vec_beam_cms.Boost(-Vec_pp35_sum.BoostVector());

    AnaDataSet ads = g_ads;
    ads.init();

    KVirtualCand * o_a = hades_tracks[trackA_num].cand;
    KVirtualCand * o_b = hades_tracks[trackB_num].cand;

    ads.fGeantWeight = 1.0;

    KVirtualCand trackA = *o_a;
    KVirtualCand trackB = *o_b;

    trackA.calc4vectorProperties(HPhysicsConstants::mass(pid_a));
    trackB.calc4vectorProperties(HPhysicsConstants::mass(pid_b));

    flag_elosscorr = 0;
    if (flag_elosscorr)
    {
        Float_t fMomA = trackA.getMomentum();
        Float_t fMomB = trackB.getMomentum();

        float momentum_A_corr = eLossCorr->getCorrMom(pid_a, fMomA, trackA.getTheta());
        float momentum_B_corr = eLossCorr->getCorrMom(pid_b, fMomB, trackB.getTheta());

        trackA.setMomentum(momentum_A_corr);
        trackB.setMomentum(momentum_B_corr);
    }

    trackA.calc4vectorProperties(HPhysicsConstants::mass(pid_a));
    trackB.calc4vectorProperties(HPhysicsConstants::mass(pid_b));

    ads.fMomAx = trackA.Px();
    ads.fMomAy = trackA.Py();
    ads.fMomAz = trackA.Pz();

    ads.fMomBx = trackB.Px();
    ads.fMomBy = trackB.Py();
    ads.fMomBz = trackB.Pz();

    ads.tr_lambda_a = trackA;
    ads.tr_lambda_b = trackB;
    ads.trec_lambda.reconstruct(trackA, trackB);
    ads.fLambda_MM = (Vec_pp35_sum - ads.trec_lambda).M2();

    // I do not need so many data!
    if (ads.trec_lambda.M() > 1200)
    {
    #ifdef SHOWREJECTED
        ads.fRejected = ERR_MASS_OUT_OF_RANGE;
    #else
        ads.ret = ERR_MASS_OUT_OF_RANGE;
        return ads;
    #endif /*SHOWREJECTED*/
    }

    if (quick_run)
    {
        ads.ret = (LAMBDA_MASS - ads.trec_lambda.M());
        return ads;// * fMinTrackDist;
    }

    ads.fLambda_MTD = ads.trec_lambda.getMTD();        // minimum distance between the two tracks
//     if (quick_run) printf(" mtd: %f\n", fMinTrackDist);

    float GeantxVertexA    = 0;
    float GeantyVertexA    = 0;
    float GeantzVertexA    = 0;
//     float GeantxVertexB    = 0;
//     float GeantyVertexB    = 0;
//     float GeantzVertexB    = 0;
#ifndef HYDRA1COMPBT
    // extra checks for the simulation analysis
    KParticleCandSim * tcs_a = dynamic_cast<KParticleCandSim*>(o_a);
    KParticleCandSim * tcs_b = dynamic_cast<KParticleCandSim*>(o_b);

    if (analysisType == KT::Sim && tcs_a && tcs_b)
    {
        KParticleCandSim trackA = *tcs_a;
        KParticleCandSim trackB = *tcs_b;

        Float_t gw_a = trackA.getGenWeight();
        Float_t gw_b = trackB.getGenWeight();

        if (gw_a != gw_b)   abort();

        ads.fGeantWeight = gw_a;

        TLorentzVector geaA; geaA.SetXYZM(trackA.getGeantxMom(), trackA.getGeantyMom(), trackA.getGeantzMom(), HPhysicsConstants::mass(pid_a));
        TLorentzVector geaB; geaB.SetXYZM(trackB.getGeantxMom(), trackB.getGeantyMom(), trackB.getGeantzMom(), HPhysicsConstants::mass(pid_b));
        TLorentzVector geaAB = geaA + geaB;
        ads.fGeaP = geaAB.P();
        ads.fGeaPx = geaAB.Px();
        ads.fGeaPy = geaAB.Py();
        ads.fGeaPz = geaAB.Pz();
        ads.fGeaTheta = geaAB.Theta() * R2D;
        ads.fGeaPhi = geaAB.Phi() * R2D;
        ads.fGeaAngleAB = geaA.Angle(geaB.Vect());

        ads.fGeaP_A = geaA.P();
        ads.fGeaP_B = geaB.P();

        TLorentzVector geaAB_cms = geaAB;
        geaAB_cms.Boost(-Vec_pp35_sum.BoostVector());

        // find plane normal
        TVector3 beam = Vec_pp35_beam.Vect();
        TVector3 lambda = geaAB.Vect();

        GeantxVertexA    = trackA.getGeantxVertex();
        GeantyVertexA    = trackA.getGeantyVertex();
        GeantzVertexA    = trackA.getGeantzVertex();
//         GeantxVertexB    = trackB.getGeantxVertex();
//         GeantyVertexB    = trackB.getGeantyVertex();
//         GeantzVertexB    = trackB.getGeantzVertex();

        int GeantPIDA = trackA.getGeantPID();
        int GeantPIDB = trackB.getGeantPID();

        int GeantPIDAparent = trackA.getGeantParentPID();
        int GeantPIDBparent = trackB.getGeantParentPID();

        int GeantPIDBGparent = trackB.getGeantGrandParentPID();

        // the simulated vertex of particleA and particleB has to be the same
        ads.fRealLambda = ( (GeantPIDA == 14 and GeantPIDAparent == 18 and GeantPIDB == 9 and GeantPIDBparent == 18) or (GeantPIDA == 14 and GeantPIDAparent == 18 and GeantPIDB == 6 and GeantPIDBparent == 9 and GeantPIDBGparent == 18));
//printf(" PIDa=%d  PIDb=%d  gPIDa=%d gPIDb=%d  -> %d\n", GeantPIDA, GeantPIDB, GeantPIDAparent, GeantPIDBparent, ads.fRealLambda);
//         if (ads.fRealLambda)
//         {
//             printf("*************** TRACK A ***************\n");
//             trackA.print(1<<4 | 1<<2);
//             printf("*************** TRACK B ***************\n");
//             trackB.print(1<<4 | 1<<2);
//         }

        Int_t info_num_a = trackA.getGenInfo();
        Int_t info_num_b = trackB.getGenInfo();

        if (info_num_a != info_num_b)   abort();
        ads.fGeantInfoNum = info_num_a;

        // FIXME
//         if (!wasLambda or (flag_nosigmas and wasSigma))
//         {
// #ifdef SHOWREJECTED
//             ads.fRejected = ERR_NO_LAMBDA;
// #else
//             ads.ret = ERR_NO_LAMBDA;
// //            return ads;
// #endif /*SHOWREJECTED*/
//         }
    }
    else
    {
        ads.fRealLambda = -1;
    }
#endif
//     if (quick_run) printf(" : %f, : %f\n", trackA.P(), trackB.P());

//             float thetaA = trackA.getTheta();
//             float thetaB = trackB.getTheta();

//             if (fMomA > 0.0 and fMomB > 0.0)
//             {
//                 float momAscale = momentum_A_corr / fMomA;
//                 float momBscale = momentum_B_corr / fMomB;
//                 trackA.SetXYZM(trackA.Px()*momAscale, trackA.Py()*momAscale, trackA.Pz() * momAscale, trackA.M());
//                 trackB.SetXYZM(trackB.Px()*momBscale, trackB.Py()*momBscale, trackB.Pz() * momBscale, trackB.M());
//             }

    ads.fChiA = trackA.getChi2();
    ads.fChiB = trackB.getChi2();
#ifndef HYDRA1COMPBT
    ads.fMetaMatchQA = trackA.getMetaMatchQuality();
    ads.fMetaMatchQB = trackB.getMetaMatchQuality();
#endif
//    if (0)
    if (ads.fMetaMatchQA > 35 || ads.fMetaMatchQB > 35)
    {
#ifdef SHOWREJECTED
        ads.fRejected = ERR_BAD_MMQ;
#else
//        ads.ret = ERR_BAD_MMQ;
//        return ads;
#endif /*SHOWREJECTED*/
    }

    ads.vx_lambda = ads.trec_lambda.getDecayVertex();

    if (analysisType == KT::Sim)
    {
        ads.fDVres = TMath::Sqrt(
                TMath::Power(ads.vx_lambda.X() - GeantxVertexA, 2) +
                TMath::Power(ads.vx_lambda.Y() - GeantyVertexA, 2) +
                TMath::Power(ads.vx_lambda.Z() - GeantzVertexA, 2)
        );
    }

    ads.fVertDistA = ads.trec_lambda.getMTDa();
    ads.fVertDistB = ads.trec_lambda.getMTDb();

    ads.fAngleAB    = trackA.Angle(trackB.Vect());
    ads.fRelAngleA  = ads.trec_lambda.Angle(trackA.Vect());
    ads.fRelAngleB  = ads.trec_lambda.Angle(trackB.Vect());

    //Boost in CMS: ***********************************************
    TLorentzVector trackAB_CMS = ads.trec_lambda;

    trackAB_CMS.Boost(-Vec_pp35_sum.BoostVector());
    ads.tr_lambda_cms = trackAB_CMS;

    TLorentzVector trackA_CMS = trackA;
    trackA_CMS.Boost(-Vec_pp35_sum.BoostVector());
    ads.tr_lambda_a_cms            = trackA_CMS;

    TLorentzVector trackB_CMS = trackB;
    trackB_CMS.Boost(-Vec_pp35_sum.BoostVector());
    ads.tr_lambda_b_cms            = trackB_CMS;

    //*************************************************************

    ads.fMt                = ads.trec_lambda.Mt();               // Transverse mass

    if (flag_nosecvertcuts == 0)
    {
    if (analysisType == KT::Sim)     //for simulated data
    {
//                 if( !(
//                      fMinTrackDist < par_Mtd/*kMTD*/
//                      && VerDistA > par_VertDistA/*kVDAB*/
//                      && VerDistB > par_VertDistB/*kVDAB*/
//                      && VerDistX > par_VertDistX/*kVDX*/
//                      &&
//                      GeantxVertexA == GeantxVertexB      // the simulated vertex of particleA and particleB has to be the same
//                      && GeantyVertexA == GeantyVertexB
//                      && GeantzVertexA == GeantzVertexB
//                     fRealLambda
//                           )
//                   )
//                 {
//                     continue;
//                 }
    }
    else       //for experimental data
    {
//                 if ( !(
//                     fMinTrackDist < par_Mtd/*kMTD*/
//                      && VerDistA > par_VertDistA/*kVDAB*/
//                      && VerDistB > par_VertDistB/*kVDAB*/
//                      && VerDistX > par_VertDistX/*kVDX*/
//                          )
//                       )
//                 {
//                     continue;
//                 }
    }
    }

//     if (analysisType == KT::Sim)     //for simulated data
//     {
//         if( !(GeantxVertexA == GeantxVertexB      // the simulated vertex of particleA and particleB has to be the same
//             && GeantyVertexA == GeantyVertexB
//             && GeantzVertexA == GeantzVertexB
//                 )
//             )
//         {
//             return ERR_SIM_VERTEX_MISSMATCH;
//         }
//     }

    ++ads.fEventLCounter;

//     A_PID = KT::pip;
//     trackA = *(KParticleCandSim*)pcand->getObject(trackA_num);
//     trackA.calc4vectorProperties(HPhysicsConstants::mass(A_PID));
//     KTifini::CalcSegVector(trackA.getZ(), trackA.getR(), trackA.getPhi(), trackA.getTheta(), baseA, dirA);
//     ads.fMomA = trackA.getMomentum();
// 
//     if (flag_elosscorr)
//     {
//         // with corr
//         momentum_A_corr = eLossCorr->getCorrMom(A_PID, ads.fMomA, trackA.getTheta());
//     }
//     else
//     {
//         // no corr
//         momentum_A_corr = ads.fMomA;
//     }
// 
//     trackA.setMomentum(momentum_A_corr);
// 
//     trackA.calc4vectorProperties(HPhysicsConstants::mass(A_PID));
// 
//     TLorentzVector trackAB_miss = trackA + trackB;
// 
//     ads.fM_miss = trackAB_miss.M();
//     ads.fPVA_miss = calcAngleVar(v1, trackAB_miss);
// 
//     ads.ret = (LAMBDA_MASS - ads.trec_lambda.M());
    return ads;
}

AnaDataSet ef_s1385p_ppippim_pp35::singleHadesPairAnalysisS1385p(HEvent* /*fEvent*/, int /*event_num*/, const AnaDataSet& ads_a, UInt_t pid_b, int trackB_num, bool /*quick_run*/)
{
    AnaDataSet ads = ads_a;

    const KTrackReconstructor & trackA = ads_a.trec_lambda;

    KVirtualCand * o_b = hades_tracks[trackB_num].cand;
    KVirtualCand trackB = *o_b;

    trackB.calc4vectorProperties(HPhysicsConstants::mass(pid_b));

    flag_elosscorr = 0;
    if (flag_elosscorr)
    {
        Float_t fMomB = trackB.getMomentum();
        float momentum_B_corr = eLossCorr->getCorrMom(pid_b, fMomB, trackB.getTheta());
        trackB.setMomentum(momentum_B_corr);
    }

    trackB.calc4vectorProperties(HPhysicsConstants::mass(pid_b));
    ads.tr_s1385p_c = trackB;

    ads.trec_s1385p.reconstruct(trackA, trackB);
    ads.vx_s1385p = ads.trec_s1385p.getDecayVertex();
    ads.fS1385p_MTD = ads.trec_s1385p.getMTD();

    // distance between S(1385)+ and Lambda
    HGeomVector v1 = ads.vx_lambda - ads.vx_s1385p;
    ads.fVertDistX = (v1).length();
    ads.fLambda_PVA = calcAngleVar(v1, ads.trec_lambda);

    if ((ads.vx_lambda.Z() - ads.vx_s1385p.Z()) < 0)
    {
#ifdef SHOWREJECTED
        ads.fRejected = ERR_VERTEX_Z_MISSMATCH;
#else
        ads.ret = ERR_VERTEX_Z_MISSMATCH;
//        return ads;
#endif /*SHOWREJECTED*/
    }

    // cms system
    TLorentzVector trackLC_CMS = ads.trec_s1385p;
    trackLC_CMS.Boost(-Vec_pp35_sum.BoostVector());
    ads.tr_s1385p_cms = trackLC_CMS;

    TLorentzVector trackC_CMS = trackB;
    trackC_CMS.Boost(-Vec_pp35_sum.BoostVector());
    ads.tr_s1385p_c_cms = trackC_CMS;

    // I do not need so many data!
    KCutInside<Float_t> s1385p_mass_test(S1385P_MASS - 200.0, S1385P_MASS + 300.0, KT::WEAK, KT::WEAK);

    if (!s1385p_mass_test.test(ads.trec_s1385p.M()))
    {
    #ifdef SHOWREJECTED
        ads.fRejected = ERR_MASS_OUT_OF_RANGE;
    #else
        ads.ret = ERR_MASS_OUT_OF_RANGE;
        return ads;
    #endif /*SHOWREJECTED*/
    }
#ifndef HYDRA1COMPBT
    // extra checks for the simulation analysis
    KParticleCandSim * tcs_b = dynamic_cast<KParticleCandSim*>(o_b);

    if (analysisType == KT::Sim && tcs_b)
    {
        TLorentzVector geaC; geaC.SetXYZM(tcs_b->getGeantxMom(), tcs_b->getGeantyMom(), tcs_b->getGeantzMom(), HPhysicsConstants::mass(pid_b));
        ads.fGeaP_C = geaC.P();

        int GeantPIDB = tcs_b->getGeantPID();
        int GeantPIDBparent = tcs_b->getGeantParentPID();
        int GeantPIDBinfo = tcs_b->getGenInfo1();

        ads.fRealS1385p = (
            // Lambda-pip pair
            (ads.fRealLambda and GeantPIDB == 8 and GeantPIDBinfo == 71) // Sigma(1385)+ PID == 71
            or
            // lambda-pip (decaying into mu-)
            (ads.fRealLambda and GeantPIDB == 5 and GeantPIDBparent == 8 and GeantPIDBinfo == 71)
        );
    }
    else
    {
        ads.fRealS1385p = -1;
    }
#endif
    ads.fS1385p_MM = (Vec_pp35_sum - ads.trec_s1385p).M2();        // Missing Mass

    ads.fChiC = trackB.getChi2();
#ifndef HYDRA1COMPBT
    ads.fMetaMatchQC = trackB.getMetaMatchQuality();
#endif
    ads.fVertDistL = ads.trec_s1385p.getMTDa();
    ads.fVertDistC = ads.trec_s1385p.getMTDb();

    ads.ret = ads.trec_s1385p.M();
    return ads;
}

void ef_s1385p_ppippim_pp35::configureTree(TTree * tree)
{
    trec_lambda.setTree(tree, "Lambda_");
    track_lambda_cms.setTree(tree, "Lambda_cms_", KTrack::bCosTheta | KTrack::bP | KTrack::bY);
    track_lambda_a.setTree(tree, "partA_", 0x1ff);
    track_lambda_a_cms.setTree(tree, "partA_cms_", KTrack::bP);
    track_lambda_b.setTree(tree, "partB_", 0x1ff);
    track_lambda_b_cms.setTree(tree, "partB_cms_", KTrack::bP);

    trec_s1385p.setTree(tree, "S1385p_");
    track_s1385p_cms.setTree(tree, "S1385p_cms_", KTrack::bCosTheta | KTrack::bP | KTrack::bY);
    track_s1385p_c.setTree(tree, "partC_", 0x1ff);
    track_s1385p_c_cms.setTree(tree, "partC_cms_", KTrack::bP);

    vertex_lambda.setTree(tree, "LambdaDecay", KVertex::bXYZ | KVertex::bR);
    vertex_s1385p.setTree(tree, "S1385pDecay", KVertex::bXYZ | KVertex::bR);

    tree->Branch("fLambda_MM",      &g_ads.fLambda_MM,      "fLambda_MM/F");
    tree->Branch("fS1385p_MM",      &g_ads.fS1385p_MM,      "fS1385p_MM/F");

    tree->Branch("fMt",             &g_ads.fMt,             "fMt/F");

    tree->Branch("fLambda_MTD",     &g_ads.fLambda_MTD,     "fLambda_MTD/F" );
    tree->Branch("fS1385p_MTD",     &g_ads.fS1385p_MTD,     "fS1385p_MTD/F" );
    tree->Branch("fVertDistX",      &g_ads.fVertDistX,      "fVertDistX/F");
    tree->Branch("fLambda_PVA",     &g_ads.fLambda_PVA,     "fLambda_PVA/F");
//     tree->Branch("fS1385p_PVA",     &g_ads.fS1385p_PVA,     "fS1385p_PVA/F");

    tree->Branch("fEventVertexX",   &g_ads.fEventVertexX,   "fEventVertexX/F");
    tree->Branch("fEventVertexY",   &g_ads.fEventVertexY,   "fEventVertexY/F");
    tree->Branch("fEventVertexZ",   &g_ads.fEventVertexZ,   "fEventVertexZ/F");

    tree->Branch("fDVres",          &g_ads.fDVres,          "fDVres/F");

    tree->Branch("fAngleAB",        &g_ads.fAngleAB,        "fAngleAB/F");
    tree->Branch("fRelAngleA",      &g_ads.fRelAngleA,      "fRelAngleA/F");
    tree->Branch("fRelAngleB",      &g_ads.fRelAngleB,      "fRelAngleB/F");

    tree->Branch("fChiA",           &g_ads.fChiA,           "fChiA/F");
    tree->Branch("fChiB",           &g_ads.fChiB,           "fChiB/F");
    tree->Branch("fChiC",           &g_ads.fChiC,           "fChiC/F");
    tree->Branch("fMetaMatchQA",    &g_ads.fMetaMatchQA,    "fMetaMatchQA/F");
    tree->Branch("fMetaMatchQB",    &g_ads.fMetaMatchQB,    "fMetaMatchQB/F");
    tree->Branch("fMetaMatchQC",    &g_ads.fMetaMatchQC,    "fMetaMatchQC/F");
    tree->Branch("fVertDistA",      &g_ads.fVertDistA,      "fVertDistA/F");
    tree->Branch("fVertDistB",      &g_ads.fVertDistB,      "fVertDistB/F");
    tree->Branch("fVertDistL",      &g_ads.fVertDistL,      "fVertDistL/F");
    tree->Branch("fVertDistC",      &g_ads.fVertDistC,      "fVertDistC/F");
    tree->Branch("fHadesTracksNum", &g_ads.fHadesTracksNum, "fHadesTracksNum/I");

    tree->Branch("fMultA",          &g_ads.fMultA,          "fMultA/I");
    tree->Branch("fMultB",          &g_ads.fMultB,          "fMultB/I");

    tree->Branch("fEventLCounter",  &g_ads.fEventLCounter,  "fEventLCounter/I");

    if (analysisType == KT::Sim)
    {
        tree->Branch("fRealLambda", &g_ads.fRealLambda,     "fRealLambda/I");
        tree->Branch("fRealS1385p", &g_ads.fRealS1385p,     "fRealS1385p/I");
        tree->Branch("fPrimLambda", &g_ads.fPrimLambda,     "fPrimLambda/I");
        tree->Branch("fGeantInfoNum",   &g_ads.fGeantInfoNum,   "fGeantInfoNum/I");
        tree->Branch("fGeantWeight",    &g_ads.fGeantWeight,"fGeantWeight/F");
    }

    tree->Branch("fWallT",          &g_ads.fWallT,          "fWallT/F");
    tree->Branch("fWallX",          &g_ads.fWallX,          "fWallX/F");
    tree->Branch("fWallY",          &g_ads.fWallY,          "fWallY/F");
    tree->Branch("fWallR",          &g_ads.fWallR,          "fWallR/F");
    tree->Branch("fWallCharge",     &g_ads.fWallCharge,     "fWallCharge/F");
    tree->Branch("fWallP",          &g_ads.fWallP,          "fWallP/F");
    tree->Branch("fWallPx",         &g_ads.fWallPx,         "fWallPx/F");
    tree->Branch("fWallPy",         &g_ads.fWallPy,         "fWallPy/F");
    tree->Branch("fWallPz",         &g_ads.fWallPz,         "fWallPz/F");
    tree->Branch("fWallBeta",       &g_ads.fWallBeta,       "fWallBeta/F");
    tree->Branch("fWallGamma",      &g_ads.fWallGamma,      "fWallGamma/F");
    tree->Branch("fWallClusterSize",&g_ads.fWallClusterSize,"fWallClusterSize/I");
    tree->Branch("fWallClustersNum",&g_ads.fWallClustersNum,"fWallClustersNum/I");

    if (analysisType == KT::Sim)
    {
    tree->Branch("fGeaP",           &g_ads.fGeaP,           "fGeaP/F");
    tree->Branch("fGeaPx",          &g_ads.fGeaPx,          "fGeaPx/F");
    tree->Branch("fGeaPy",          &g_ads.fGeaPy,          "fGeaPy/F");
    tree->Branch("fGeaPz",          &g_ads.fGeaPz,          "fGeaPz/F");
    tree->Branch("fGeaTheta",       &g_ads.fGeaTheta,       "fGeaTheta/F");
    tree->Branch("fGeaPhi",         &g_ads.fGeaPhi,         "fGeaPhi/F");
    tree->Branch("fGeaAngleAB",     &g_ads.fGeaAngleAB,     "fGeaAngleAB/F");
    tree->Branch("fGeaP_A",         &g_ads.fGeaP_A,         "fGeaP_A/F");
    tree->Branch("fGeaP_B",         &g_ads.fGeaP_B,         "fGeaP_B/F");
    tree->Branch("fGeaP_C",         &g_ads.fGeaP_C,         "fGeaP_C/F");
    }

    tree->Branch("fPVtype",         &g_ads.fPVtype,         "fPVtype/I");

#ifdef SHOWREJECTED
    tree->Branch("fRejected",       &g_ads.fRejected,       "fRejected/I");
#endif /*SHOWREJECTED*/

    tree->Branch("fSortOrderMtd",   &g_ads.fSortOrderMtd,   "fSortOrderMtd/I");
}

void ef_s1385p_ppippim_pp35::configureGraphicalCuts(KTrackInspector & trackInsp)
{
//    const TString jsieben_pNb_cuts = "/scratch/e12f/knucl/jsieben/pNb/Cuts/";
//    const TString aschmah_pp35_cuts = "/scratch/e12f/schmah/GraphicalCuts/pp35/";
//    const TString jchen_pp35_cuts = "/home/gu27buz/hadesdst/pp35/";
//    const TString jchen_pp35_cuts_sim = "/scratch/e12l/knucl/hades/jchen/pp35/GraphicalCuts/Sim/";
//    const TString jchen_pp35_cuts_sim2 = "/scratch/e12f/knucl/rlalik/pp35/LambdaAnalysis/graph_cuts/";//"/scratch/e12l/knucl/hades/jchen/pp35/new_backup/GraphicalCuts/Sim/";

//    setChargePID(kTRUE);

//    const TString rlalik_cuts = "/scratch/e12m/knucl/rlalik/pp35/LambdaAnalysis/Exp/";
    if (analysisType == KT::Sim)
    {
        // protons
        trackInsp.registerCut(KT::DEDX, KT::MDC, KT::cut_p,  "bindata/pp35_p_cut.root", "Mdc_dEdx_P_cut_mod_ChiiV1_Sim_mod");
        // pions-
        trackInsp.registerCut(KT::DEDX, KT::MDC, KT::cut_pim, "bindata/pp35_pim_cut.root", "Mdc_dEdx_PiP_cut_PID_mod_ChiiV2_Sim_mod_PiM");
        trackInsp.registerCut(KT::DEDX, KT::MDC, KT::cut_pip, "bindata/pp35_pim_cut.root", "Mdc_dEdx_PiP_cut_PID_mod_ChiiV2_Sim_mod_PiM", true);
    }
    else if (analysisType == KT::Exp)
    {
        // protons
        trackInsp.registerCut(KT::DEDX, KT::MDC, KT::cut_p, "bindata/Mdc_dEdx_P_cut_mod_ChiiV1.root", "Mdc_dEdx_P_cut_mod_ChiiV1");
        // pions-
        trackInsp.registerCut(KT::DEDX, KT::MDC, KT::cut_pim, "bindata/Mdc_dEdx_PiP_cut_PID_mod_ChiiV2.root", "Mdc_dEdx_PiP_cut_PID_mod_ChiiV2", kFALSE);    // new cat is already for pim
        // pions+
        trackInsp.registerCut(KT::DEDX, KT::MDC, KT::cut_pip, "bindata/Mdc_dEdx_PiP_cut_PID_mod_ChiiV2.root", "Mdc_dEdx_PiP_cut_PID_mod_ChiiV2", kTRUE);    // new cat is already for pim
    }

    trackInsp.configureMetaSystem(KT::cut_p, KT::MDC);
    trackInsp.configureMetaSystem(KT::cut_pim, KT::MDC);
    trackInsp.configureMetaSystem(KT::cut_pip, KT::MDC);
}

void ef_s1385p_ppippim_pp35::initAnalysis(KT::Experiment exp, KT::AnalysisType analysisType)
{
    KAbstractAnalysis::initAnalysis(exp, analysisType);

    refBeamVector = getTargetGeomVector();
    beamCal = new KBeamCalibrationApr07(getTargetGeometry());
    beamCal->initBeamCorrArray(analysisType);
}

void ef_s1385p_ppippim_pp35::finalizeAnalysis()
{
    delete beamCal;
    KAbstractAnalysis::finalizeAnalysis();
}

void ef_s1385p_ppippim_pp35::ConfigureOptions(int& n, option* & long_options)
{
    static struct option _long_options[] =
    {
        /* These options set a flag. */
        { "nosecvercuts",       no_argument,        &flag_nosecvertcuts,    1 },
        { "elosscorr",          no_argument,        &flag_elosscorr,        1 },
        { "no-sigmas",          no_argument,        &flag_nosigmas,         1 },
        { "use-event-vertex",   no_argument,        &flag_useeventvertex,   1 },
        { "use-wall",           no_argument,        &flag_usewall,          1 },
        /* These options don't set a flag.
        We distingu*ish them by their indices. */
//            {"events",     no_argument,       0, 'e'},
        { "Mtd",                required_argument,  0, 'm'},
        { "VertDistX",          required_argument,  0, 'x'},
        { "VertDistA",          required_argument,  0, 'p'},
        { "VertDistB",          required_argument,  0, 'q'},
    };

    long_options = _long_options;
    n = sizeof(_long_options)/sizeof(option);
}

int ef_s1385p_ppippim_pp35::Configure(int c, const char * optarg)
{
    switch (c) {
        case 'm':
            par_Mtd = atol(optarg);
            break;
        case 'x':
            par_VertDistX = atol(optarg);
            break;
        case 'p':
            par_VertDistA = atol(optarg);
            break;
        case 'q':
            par_VertDistB = atol(optarg);
            break;
        case 'h':
        case '?':
            /* getopt_long already printed an error message. */
            Usage();
            break;

        default:
            abort();
            break;
    }

    return c;
}

void ef_s1385p_ppippim_pp35::Usage() const
{
    std::cout <<
    "Analysis options: \n" <<
    "      --nosecvercuts\t\t\t - disable secondary vertex cuts\n" <<
    "\n\n";
}

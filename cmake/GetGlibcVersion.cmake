file(WRITE ${CMAKE_CURRENT_BINARY_DIR}/glibc_version.c
    "#include <stdio.h>\n\ 
    #include <stdlib.h>\n\
    #include <gnu/libc-version.h>\n\
    int main(int argc, char *argv[]) {\n\
       printf(\"%s\\n\", gnu_get_libc_version());\n\
       return 0;\n\
    }"
)

try_run(GLIBC_VERSION_RUN GLIBC_VERSION_COMPILE
        ${CMAKE_CURRENT_BINARY_DIR} ${CMAKE_CURRENT_BINARY_DIR}/glibc_version.c
        COMPILE_OUTPUT_VARIABLE GLIBC_VERSION_COMPILE_OUT
        RUN_OUTPUT_VARIABLE GLIBC_VERSION_TEXT
)

if (NOT GLIBC_VERSION_COMPILE)
message(STATUS "Glibc Version Compile: ${GLIBC_VERSION_COMPILE}")
message(STATUS "Glibc Version Compile Output: ${GLIBC_VERSION_COMPILE_OUT}")
endif()
if (GLIBC_VERSION_RUN)
    message(STATUS "Glibc Version Run: ${GLIBC_VERSION_RUN}")
endif()

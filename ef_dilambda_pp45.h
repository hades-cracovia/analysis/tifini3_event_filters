/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Rafał Lalik <rafal.lalik@@ph.tum.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef PP45_DILAMBDA_H
#define PP45_DILAMBDA_H

#include "henergylosscorrpar.h"

#include "Tifini3Config.h"

#include "KAbstractAnalysis.h"
#include "KTifiniAnalysis.h"
#include "KBeamCalibration.h"
#include "KVertex.h"
#include "KVector.h"
#include "KTrack.h"
#include "KTrackReconstructor.h"
#include "KCutInside.h"

// constants
static const Float_t NULL_DATA = -10000.;
static const Float_t MASS_OK = 10000.;
static const Float_t ERR_NOT_IN_VERTEX = -10001.;
static const Float_t ERR_MASS_OUT_OF_RANGE = -10002.;
static const Float_t ERR_VERTEX_Z_MISSMATCH = -10004.;
static const Float_t ERR_SIM_VERTEX_MISSMATCH = -10005.;
static const Float_t ERR_NO_FWDET_TOF = -10006.;

//#define SHOWREJECTED

struct AnaDataSet
{
    // Kinematic properties
    Float_t fLambda_MM[3];

    // track-reco properties
    Float_t fLambda_MTD[3];
    Float_t fVertDistX[2];
    Float_t fLambda_PVA[2];

    // vertex properties
    Float_t fFitVertexX;
    Float_t fFitVertexY;
    Float_t fFitVertexZ;

    Float_t fEventVertexX;
    Float_t fEventVertexY;
    Float_t fEventVertexZ;

    Float_t fDVres;

    // daughter properties
//     Float_t fMomAx, fMomAy, fMomAz, fMomBx, fMomBy, fMomBz, fMomCx, fMomCy, fMomCz;
    Float_t fMomA_cms[2], fMomB_cms[2];
    Float_t fAngleAB;
    Float_t fRelAngleA, fRelAngleB;
    Float_t fChiA[2], fChiB[2];
    Float_t fMetaMatchQA[2], fMetaMatchQB[2];
    Float_t fVertDistA[2], fVertDistB[2];
    Int_t fMultA[2], fMultB[2];
    Float_t fpartB_tof[2];
    Int_t fpartB_tof_pid[2];

    // event stats
    Int_t fHadesTracksNum;
    Int_t fFwDetTracksNum;
    Int_t fIsFwDetData[2];
    Int_t fEventLCounter;
    Int_t fRealLambda[2];
    Int_t fPrimLambda[2];
    Int_t fCreationMech[2];
    Int_t fDecayMech[2];

    // other
    Int_t fGeantInfoNum;
    Float_t fGeantWeight;

    Float_t ret;
    Int_t fSortOrderMtd;
#ifdef SHOWREJECTED
    Int_t fRejected;
#endif

    // Geant
    Float_t fGeaP, fGeaPx, fGeaPy, fGeaPz;
    Float_t fGeaTheta, fGeaPhi;
    Float_t fGeaXf;
    Float_t fGeaAngleAB;

    Int_t fPVtype;

    KTrack tr_lambda_cms[2];
    KTrack tr_lambda_a[2];
    KTrack tr_lambda_b[2];

    KVertex vx_lambda[2];
    KVertex vx_primary;

    KTrackReconstructor trec_lambda[2];
    KTrackReconstructor trec_lala;

    void clear()
    {
        fHadesTracksNum = 0;
        fFwDetTracksNum = 0;
        fIsFwDetData[0] = fIsFwDetData[1] = 0;

        fGeantWeight = 1.0;
        fEventVertexX = 0.0;
        fEventVertexY = 0.0;
        fEventVertexZ = 0.0;

        fEventLCounter = 0;

        fMultA[0] = fMultA[1] = 0;
        fMultB [0] = fMultB[1] = 0;
    }

    void init()
    {
        fLambda_MM[0] = fLambda_MM[1] = fLambda_MM[2] = NULL_DATA;
        fLambda_MTD[0] = fLambda_MTD[1] = fLambda_MTD[2] = NULL_DATA;
        fVertDistX[0] = fVertDistX[1] = NULL_DATA;
        fLambda_PVA[0] = fLambda_PVA[1] = NULL_DATA;

        fFitVertexX = NULL_DATA;
        fFitVertexY = NULL_DATA;
        fFitVertexZ = NULL_DATA;

        fDVres = NULL_DATA;

//         fMomAx = fMomAy = fMomAz = fMomBx = fMomBy = fMomBz = fMomCx = fMomCy = fMomCz = NULL_DATA;
        fMomA_cms[0] = fMomB_cms[0] = fMomA_cms[1] = fMomB_cms[1] = NULL_DATA;

        fAngleAB = NULL_DATA;
        fRelAngleA = fRelAngleB = NULL_DATA;
        fChiA[0] = fChiB[0] = fChiA[1] = fChiB[1] = NULL_DATA;
        fMetaMatchQA[0] = fMetaMatchQB[0] = fMetaMatchQA[1] = fMetaMatchQB[1] = NULL_DATA;
        fVertDistA[0] = fVertDistB[0] = fVertDistA[1] = fVertDistB[1] = NULL_DATA;
        fpartB_tof[0] = fpartB_tof[1] = NULL_DATA;
        fpartB_tof_pid[0] = fpartB_tof_pid[1] = NULL_DATA;

        fGeantInfoNum = 0;
        fRealLambda[0] = NULL_DATA;
        fRealLambda[1] = NULL_DATA;
        fPrimLambda[0] = NULL_DATA;
        fPrimLambda[1] = NULL_DATA;
        fCreationMech[0] = NULL_DATA;
        fCreationMech[1] = NULL_DATA;
        fDecayMech[0] = NULL_DATA;
        fDecayMech[1] = NULL_DATA;

        fGeaP = fGeaPx = fGeaPy = fGeaPz = NULL_DATA;
        fGeaTheta = fGeaPhi = NULL_DATA;
        fGeaXf = NULL_DATA;
        fGeaAngleAB = NULL_DATA;

        fPVtype = NULL_DATA;

#ifdef SHOWREJECTED
        fRejected = 0;
#endif /*SHOWREJECTED*/

        ret = 0.;
        fSortOrderMtd = 0;
    }
};

class ef_dilambda_pp45 : public KAbstractAnalysis
{
public:
    ef_dilambda_pp45(const TString & analysisName, const TString & treeName);
    virtual bool analysis(HEvent * fEvent, Int_t event_num, Int_t run_id);

    void initAnalysis(KT::Experiment exp, KT::AnalysisType analysisType);
    void finalizeAnalysis();

    void ConfigureOptions(int & n, option * & long_options);
    int Configure(int c, const char * optarg);
    void Usage() const;

protected:
    void configureTree(TTree * tree);
    void configureGraphicalCuts(KTrackInspector & cuts);

    AnaDataSet singleHadesPairAnalysis(HEvent * fEvent, Int_t event_num, const AnaDataSet & ads_a, UInt_t pid_a, UInt_t pid_b, int trackA_num, int trackB_num, int ln, bool quick_run = false);
    AnaDataSet singleFwDetPairAnalysis(HEvent * fEvent, Int_t event_num, const AnaDataSet & ads_a, UInt_t pid_a, UInt_t pid_b, int trackA_num, int trackB_num, int ln, bool quick_run = false);

    HEnergyLossCorrPar * eLossCorr;
    KBeamCalibration * beamCal;
    HGeomVector refBeamVector;
    HGeomVector beamVector;
    TLorentzVector Vec_pp45_sum;

    // opts
    int flag_nosecvertcuts;
    int flag_elosscorr;
    int flag_nosigmas;
    int flag_useeventvertex;
    int flag_usewall;
    int flag_refit_fwdet;

    int par_Mtd;			// CLI
    int par_VertDistX;		// CLI
    int par_VertDistA;		// CLI
    int par_VertDistB;		// CLI

    KTrack track_lambda_cms[2];
    KTrack track_lambda_a[2];
    KTrack track_lambda_b[2];
    KVertex vertex_lambda[2];
    KVertex vertex_primary;

    KTrackReconstructor trec_lambda[2];
    KTrackReconstructor trec_lala;
};

#endif // PP45_DILAMBDA_H
